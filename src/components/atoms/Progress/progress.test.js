import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import Progress from './progress'
// Test Library
import { render, cleanup } from 'app/main/components/organism/Workflow_original/node_modules/app/main/pages/Workflow_original/node_modules/@testing-library/react'
import 'app/main/components/organism/Workflow_original/node_modules/app/main/pages/Workflow_original/node_modules/@testing-library/dom'
import 'app/main/components/organism/Workflow_original/node_modules/app/main/pages/Workflow_original/node_modules/@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Progress></Progress>, div)
})
// Props to send component to be rendered
const props = {
  properyName: 'Value',
}
test('Render correctly', () => {
  const { getByTestId } = render(<Progress {...props}></Progress>)
  expect(getByTestId('ProgressTestId')).toBeInTheDocument()
})
