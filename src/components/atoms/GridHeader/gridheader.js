import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS
import { FormattedMessage } from "react-intl";
import { Typography } from "@material-ui/core";

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const GridHeaderAtom = React.forwardRef((props, ref) => {
  const { title, description, ...rest } = props;

  return (
    /*
     @prop data-testid: Id to use inside gridheader.test.js file.
     */
    <div data-testid={"GridHeaderTestId"} ref={ref}>
      <Typography variant="h6">
        <FormattedMessage id={"some.id"} defaultMessage={props.title} />
      </Typography>
      <br />
      <Typography variant="body2">
        <FormattedMessage id={"some.id"} defaultMessage={props.description} />
      </Typography>
      <br />
    </div>
  );
});
// Type and required properties
GridHeaderAtom.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};
// Default properties
GridHeaderAtom.defaultProps = {};

export default GridHeaderAtom;
