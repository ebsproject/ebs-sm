import React from 'react'
import PropTypes from 'prop-types'
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));

const TexFieldAtom = React.forwardRef((props, ref) => {
  // Properties of the atom
  const { color, children, ...rest } = props
  const classes = useStyles();
  return (
    <form className={classes.root} noValidate autoComplete="off">
      <div
      data-testid={'TexFieldTestId'}
      >
      <TextField
              id="standard-read-only-input"
              label={props.label}
              defaultValue={props.value}
              InputProps={{
                readOnly: props.disable,
              }}
              variant="outlined"  
            />
            
              </div>
    </form>
  )
})
// Type and required properties
TexFieldAtom.propTypes = {
  color: PropTypes.oneOf([
    'primary',
    'info',
    'success',
    'warning',
    'danger',
    'transparent',
  ]),
  children: PropTypes.node.isRequired,
}
// Default properties
TexFieldAtom.defaultProps = {
  color: 'primary',
}

export default TexFieldAtom
