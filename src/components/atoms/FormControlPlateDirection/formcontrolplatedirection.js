import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';

import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));
const FormControlPlateDirectionAtom = React.forwardRef((props, ref) => {
  // Properties of the atom
  const { color, children, ...rest } = props
  const classes = useStyles();
  const [valueId, setvalueId] = React.useState(1);
  const handleChange = (event) => {
    setvalueId(event.target.value);
    props.setIdPlate(event.target.value);
  };
  return (
    <div
    data-testid={'FormControlPlateDirectionTestId'}
    >
    <FormControl variant="outlined" className={classes.formControl}>
        <InputLabel htmlFor="outlined-age-native-simple">{props.label}</InputLabel>
        <Select
          native
          value={valueId}
          onChange={handleChange}
          label={props.label}
          inputProps={{
            name: 'FormControl',
            id: 'outlined-age-native-simple',
          }}
        >
          <option value={1}>Column</option>
          <option value={2}>Row</option>
        </Select>
      </FormControl>
      </div>
  )
})
// Type and required properties
FormControlPlateDirectionAtom.propTypes = {
  color: PropTypes.oneOf([
    'primary',
    'info',
    'success',
    'warning',
    'danger',
    'transparent',
  ]),
  children: PropTypes.node.isRequired,
}
// Default properties
FormControlPlateDirectionAtom.defaultProps = {
  color: 'primary',
}

export default FormControlPlateDirectionAtom
