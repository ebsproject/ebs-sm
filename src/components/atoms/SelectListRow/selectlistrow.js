import React, { useState } from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS
import { Checkbox } from '@material-ui/core'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const SelectListRowAtom = React.forwardRef((props, ref) => {
  const { rowData, getEntryTabData, ...rest } = props

  const [checked, setChecked] = useState(false)

  const handleChange = (event) => {
    /**
     * This part of the function is to use the listDbId to get the
     * members and get the number of entries. The total number
     * of entries is needed for create request service.
     *
     * Uncomment this later when the plot data is now available.
     * Also make this function into an async. For now the data
     * will now be hard coded.
     */
    // const response = await b4rInstance.get(`/lists/${rowData.listDbId}/members`)
    // const data = response.data.result.data[0].members
    // const totalNumberEntries = data.length

    setChecked(event.target.checked)
    const totalNumberOfEntries = 3
    getEntryTabData(
      event.target.checked,
      rowData.listDbId,
      totalNumberOfEntries,
      rowData.name
    )
  }

  return (
    /*
     @prop data-testid: Id to use inside selectlistrow.test.js file.
     */
    <Checkbox checked={checked} onChange={handleChange} color="primary" />
  )
})
// Type and required properties
SelectListRowAtom.propTypes = {}
// Default properties
SelectListRowAtom.defaultProps = {}

export default SelectListRowAtom
