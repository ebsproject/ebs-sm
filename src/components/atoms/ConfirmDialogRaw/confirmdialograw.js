import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS
import {
  Button,
  DialogTitle,
  DialogContent,
  DialogActions,
  Dialog,
  Typography,
  Slide,
} from "@material-ui/core";
import { FormattedMessage } from "react-intl";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ConfirmDialogRawAtom = React.forwardRef((props, ref) => {
  const { open, onCancel, children, title, onOk, ...other } = props;

  return (
    <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      maxWidth="xs"
      fullWidth
      TransitionComponent={Transition}
      aria-labelledby="confirmation-dialog-title"
      open={open}
      {...other}
      data-testid={"ConfirmDialogRawTestId"}
    >
      <DialogTitle id="confirmation-dialog-title">
        <Typography align="left" variant="h4">
          {title}
        </Typography>
      </DialogTitle>
      <DialogContent dividers>{children}</DialogContent>
      <DialogActions>
        <Button
          autoFocus
          onClick={onCancel}
          color="secondary"
          variant="contained"
        >
          <FormattedMessage
            id="cs.confirmdialog.cancel"
            defaultMessage="Cancel"
          />
        </Button>
        <Button onClick={onOk} color="primary" variant="contained">
          <FormattedMessage
            id="cs.confirmdialog.confirm"
            defaultMessage="Confirm"
          />
        </Button>
      </DialogActions>
    </Dialog>
  );
});
// Type and required properties
ConfirmDialogRawAtom.propTypes = {
  title: PropTypes.node,
  open: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onOk: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
};
// Default properties
ConfirmDialogRawAtom.defaultProps = {
  title: "",
  open: false,
  onCancel: () => {},
  onOk: () => {},
};

export default ConfirmDialogRawAtom;
