import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import { Typography, Collapse, Snackbar, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Alert, AlertTitle } from '@material-ui/lab';
import { FormattedMessage } from 'react-intl';
import CloseIcon from '@material-ui/icons/Close';
import { useDispatch } from 'react-redux';

function CustomAlert(props) {
  return <Alert elevation={6} variant='filled' {...props} />;
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const AlertNotificationAtom = React.forwardRef((props, ref) => {
  const classes = useStyles();
  const { open, severity, message, strongMessage, translationId, ...rest } = props;
  const dispatch = useDispatch();

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    dispatch({
      type: 'CLOSE_ALERT',
    });
  };

  if (severity && message) {
    return (
      /* 
     @prop data-testid: Id to use inside alertnotification.test.js file.
     */
      <div
        className={classes.root}
        data-testid={'AlertNotificationTestId'}
        ref={ref}
      >
        <Snackbar
          open={open}
          autoHideDuration={3000}
          onClose={handleClose}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          action={
            <IconButton
              aria-label='close'
              color='inherit'
              size='small'
              onClick={handleClose}
            >
              <CloseIcon fontSize='inherit' />
            </IconButton>
          }
        >
          <CustomAlert onClose={handleClose} severity={severity}>
            <AlertTitle>
              <Typography variant='body1'>
                <FormattedMessage
                  id={`core.alert.${severity}`}
                  defaultMessage={severity.toUpperCase()}
                />
              </Typography>
            </AlertTitle>
            <FormattedMessage id={translationId} defaultMessage={message} />
            {strongMessage && (
              <strong>
                —{' '}
                <FormattedMessage
                  id={`${translationId}.strong`}
                  defaultMessage={strongMessage}
                />
              </strong>
            )}
          </CustomAlert>
        </Snackbar>
      </div>
    );
  } else {
    return <></>;
  }
});
// Type and required properties
AlertNotificationAtom.propTypes = {
  severity: PropTypes.string,
  message: PropTypes.string,
  strongMessage: PropTypes.string,
};
// Default properties
AlertNotificationAtom.defaultProps = {
  translationId: 'core.alert.message',
};

export default AlertNotificationAtom;
