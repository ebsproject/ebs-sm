import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS
import Button from '@material-ui/core/Button'
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ButtonDownloadAtom = React.forwardRef((props, ref) => {
  // Properties of the atom
  const { color, children, ...rest } = props

  const createReport = () => {
    props.action(props.row);
  };

  return (
    /* 
     @prop data-testid: Id to use inside buttondownload.test.js file.
     */
    <Button
      data-testid={'ButtonDownloadTestId'}
    
        //id = {`test-validate-${props.row.original.id}`}
        justIcon
        round
        simple
        onClick={createReport}
        color="primary"
        >
            <CloudDownloadIcon />
    
    
    </Button>
  )
})
// Type and required properties
ButtonDownloadAtom.propTypes = {
  color: PropTypes.oneOf([
    'primary',
    'info',
    'success',
    'warning',
    'danger',
    'transparent',
  ]),
  children: PropTypes.node.isRequired,
}
// Default properties
ButtonDownloadAtom.defaultProps = {
  color: 'primary',
}

export default ButtonDownloadAtom
