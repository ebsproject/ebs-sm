import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS
import {
  Grid,
  Button,
  DialogActions,
  Typography,
} from '@material-ui/core'
import EbsForm from 'ebs-form'
import { FormattedMessage } from 'react-intl'
import { useQuery } from '@apollo/client'
import { FIND_CROP_LIST } from 'utils/apollo/gql/RequestManager'
import { B4Rrest } from 'utils/axios'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const GeneralFormAtom = React.forwardRef((props, ref) => {
  const {
    general,
    value,
    irri,
    cimmyt,
    requester,
    setRequester,
    requesterEmail,
    onSubmitCimmyt,
    handleChange,
    serviceProviderForm,
    basicInformationOfServiceForm,
    ...rest
  } = props
  const [crops, setCrops] = React.useState(null)
  const [switchLabel, setSwitchLabel] = React.useState(
    ' Place requester information as Admin'
  )
  const [creatorDbId, setCreatorDbId] = useState(null)
  const [programDbId, setProgramDbId] = useState(null)
  const [adminContactEmail, setAdminContactEmail] = React.useState(null)
  const [adminContactName, setAdminContactName] = React.useState(
    cimmyt ? cimmyt.adminContactName : null
  )
  const [checked, setChecked] = React.useState(cimmyt ? cimmyt.switch2 : false)
  const [formId, setFormId] = React.useState(irri?.researcher ? 2 : 1)

  useEffect(() => {
    getUserProfile(requesterEmail)
  }, [])

  const cropsData = useQuery(FIND_CROP_LIST)
  if (!crops && cropsData.data) {
    let crops = []
    cropsData.data.findCropList.content.map((crop) => {
      crops.push({ value: crop.id, label: crop.cropName })
    })
    setCrops(crops)
  }

  const switchChanged = (item, setValue) => {
    setChecked(item)

    if (item) {
      setSwitchLabel('')
      setValue('adminContactEmail', requesterEmail)
      setValue('adminContactName', creatorDbId)
    } else {
      setSwitchLabel(' Place requester information as Admin')
      setValue('adminContactEmail', '')
      setValue('adminContactName', '')
    }
  }

  async function getUserProfile(email) {
    /**
     * This for getting the creatorDbId of the logged in user using
     * the email. creatorDbId then will be used to get the lists
     * under the logged in user.
     */
    const body = {
      fields:
        'person.email | person.username | person.first_name AS firstName | person.last_name AS lastName | person.person_name AS personName | person.creator_id AS creatorDbId',
      email: `equals ${email}`,
    }

    const response = await B4Rrest.post('/persons-search', body)
    if (response) {
      setCreatorDbId(response.data.result.data[0]?.personName)
      setRequester(response.data.result.data[0]?.personName)
      // getUserProgram(response.data.result.data[0].creatorDbId)
    }
  }

  // async function getUserProgram(id) {
  //   /**
  //    * This for getting the creatorDbId of the logged in user using
  //    * the email. creatorDbId then will be used to get the lists
  //    * under the logged in user.
  //    */

  //   const response = await B4Rrest.get(`/persons/${id}/programs`)
  //   if (response) {
  //     setProgramDbId(response.data.result.data[0].setProgramDbId)
  //   }
  // }

  const brandColor = '#808080'

  const customStyles = {
    control: (base, state) => ({
      ...base,
      boxShadow: state.isFocused ? 0 : 0,
      borderColor: state.isFocused ? brandColor : base.borderColor,
      color: state.isDisabled ? base.borderColor : brandColor,
      '&:hover': {
        borderColor: state.isFocused ? brandColor : base.borderColor,
      },
      '&:disabled': {
        color: brandColor,
      },
    }),
  }

  const cimmytDefinition = (props) => {
    const { getValues, setValue, reset } = props
    return {
      name: 'cimmytForm',
      components: [
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'Switch',
          name: 'switch2',
          label: switchLabel,
          onChange: (e) => switchChanged(e.target.checked, setValue),
          defaultValue: checked,
          inputProps: {
            size: 'small',
            disabled: false,
            color: 'primary',
          },
          helper: {
            title: "Is the admin information the same as the requester's?",
            placement: 'right',
            arrow: true,
          },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'TextField',
          name: 'adminContactName',
          inputProps: {
            size: 'small',
            color: 'primary',
            label: 'Admin Contact',
          },
          defaultValue: cimmyt && cimmyt.adminContactName,
          helper: {
            title: 'The name of the admin person to contact about the request.',
            placement: 'right',
            arrow: true,
          },
          rules: { required: 'Please enter a value for Admin Contact' },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'TextField',
          name: 'adminContactEmail',
          inputProps: {
            size: 'small',
            color: 'primary',
            label: 'Admin Email',
          },
          defaultValue: cimmyt && cimmyt.adminContactEmail,
          helper: {
            title: "Admin Contact's email address",
            placement: 'right',
            arrow: true,
          },
          rules: {
            validate: (value) => {
              const emailExpresion = new RegExp('^[^@]+@[^@]+.[a-zA-Z]{2,}$')
              return emailExpresion.test(value)
                ? true
                : '⚠ Invalid email format'
            },
            required: 'Please enter a value for Admin Email',
          },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'TextField',
          name: 'icc',
          inputProps: {
            size: 'small',
            color: 'primary',
            label: 'ICC',
          },
          defaultValue: cimmyt && cimmyt.icc,
          helper: {
            title: 'Account to charge the cost of the service.',
            placement: 'right',
            arrow: true,
          },
          rules: { required: 'Please enter a value for ICC' },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'DatePicker',
          name: 'submitionDate',
          defaultValue: cimmyt && cimmyt.submitionDate,
          inputProps: {
            size: 'small',
            autoOk: true,
            variant: 'inline',
            label: 'Submission Date',
            disabled: true,
            format: 'MM/dd/yyyy',
          },
          rules: {},
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'DatePicker',
          name: 'completedBy',
          inputProps: {
            size: 'small',
            autoOk: true,
            variant: 'inline',
            label: 'Completed By',
            format: 'MM/dd/yyyy',
            minDate: getValues('submitiondate') || new Date(),
          },
          defaultValue: cimmyt && cimmyt.completedBy,
          helper: {
            title: 'Target date when the request should be completed',
            placement: 'right',
            arrow: true,
          },
          rules: { required: 'Please enter target completion date' },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'TextField',
          name: 'description',
          defaultValue: cimmyt && cimmyt.description,
          inputProps: {
            size: 'small',
            variant: 'outlined',
            color: 'primary',
            label: 'Notes',
            multiline: true,
            rows: 4,
          },
        },
      ],
    }
  }

  const irriDefinition = (props) => {
    const { getValues, setValue, reset } = props
    return {
      name: 'irriForm',
      components: [
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'TextField',
          name: 'researcher',
          inputProps: {
            size: 'small',
            color: 'primary',
            label: 'Researcher',
          },
          defaultValue: irri && irri.researcher,
          helper: {
            title: 'The name of the researcher.',
            placement: 'right',
            arrow: true,
          },
          rules: { required: 'Please enter a value for Researcher' },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'TextField',
          name: 'ocsnumber',
          inputProps: {
            color: 'primary',
            size: 'small',
            label: 'OCS Number',
          },
          defaultValue: irri && irri.ocsnumber,
          helper: {
            title: 'Please enter the ocs number',
            placement: 'right',
            arrow: true,
          },
          rules: { required: 'Please enter a value for OCS Number' },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'DatePicker',
          name: 'completedBy',
          inputProps: {
            size: 'small',
            autoOk: true,
            variant: 'inline',
            label: 'Completed By',
            format: 'MM/dd/yyyy',
            minDate: getValues('submitiondate') || new Date(),
          },
          defaultValue: irri && irri.completedBy,
          helper: {
            title: 'Target date when the request should be completed',
            placement: 'right',
            arrow: true,
          },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'TextField',
          name: 'contactperson',
          inputProps: {
            size: 'small',
            color: 'primary',
            label: 'Contact Person',
          },
          defaultValue: irri && irri.contactperson,
          helper: {
            title: 'The name of the person to contact about the request.',
            placement: 'right',
            arrow: true,
          },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'TextField',
          name: 'description',
          defaultValue: irri && irri.description,
          inputProps: {
            variant: 'outlined',
            color: 'primary',
            label: 'Notes',
            multiline: true,
            rows: 4,
          },
        },
      ],
    }
  }

  const generalDefinition = (props) => {
    const { getValues, setValue, reset } = props
    return {
      name: 'generalForm',
      components: [
        // {
        //   sizes: [12, 12, 12, 12, 12],
        //   component: "Select",
        //   name: "crop",
        //   options: crops,
        //   inputProps: {
        //     isDisabled: false,
        //     label: "Crop",
        //     placeholder: "Crop",
        //     variant: "standard",
        //     styles: customStyles
        //   },
        //   defaultValue: general && general.crop,
        //   onChange: (e) => getRequestCode(setValue),
        //   helper: {
        //     title:
        //       "Cultivated plant that is grown as food and is the subject of research and development conducted by various programs",
        //     placement: "right",
        //     arrow: true,
        //   },
        //   rules: { required: "Crop is required. Please select one." },
        // },

        {
          sizes: [12, 12, 12, 12, 12],
          component: 'TextField',
          name: 'program',
          inputProps: {
            size: 'small',
            color: 'primary',
            label: 'Program',
            disabled: true,
          },
          defaultValue: 'BW Wheat Breeding Program',
          helper: {
            title: 'Program',
            placement: 'right',
            arrow: true,
          },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'TextField',
          name: 'crop',
          inputProps: {
            size: 'small',
            color: 'primary',
            label: 'Crop',
            placeholder: ' ',
            disabled: true,
          },
          defaultValue: 'Wheat',
          helper: {
            title: 'Should be autopopulated',
            placement: 'right',
            arrow: true,
          },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'TextField',
          name: 'requester',
          inputProps: {
            size: 'small',
            color: 'primary',
            label: 'Requester',
            placeholder: creatorDbId,
            disabled: true,
          },
          defaultValue: requester,
          helper: {
            title: 'The person who is making the request',
            placement: 'right',
            arrow: true,
          },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'TextField',
          name: 'requesterEmail',
          inputProps: {
            size: 'small',
            color: 'primary',
            label: "Requester's Email",
            placeholder: requesterEmail,
            disabled: true,
          },
          defaultValue: requesterEmail,
          helper: {
            title: "Requester's email address",
            placement: 'right',
            arrow: true,
          },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'TextField',
          name: 'tissuetype',
          inputProps: {
            size: 'small',
            color: 'primary',
            label: 'Tissue Type',
            placeholder: ' ',
            disabled: true,
          },
          defaultValue: 'Leaf',
          helper: {
            title: 'Tissue Type',
            placement: 'right',
            arrow: true,
          },
        },
      ],
    }
  }

  return (
    /*
     @prop data-testid: Id to use inside generalform.test.js file.
     */

    <div>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <Typography variant="body1">
            <FormattedMessage
              id={'some.id'}
              defaultMessage={
                'Please provide the basic information of the service:'
              }
            />
          </Typography>
          <br />
          <div data-testid={'GeneralFormTestId'} ref={ref}>
            <EbsForm definition={generalDefinition}></EbsForm>
          </div>
        </Grid>
        <Grid item xs={6}>
          <div>
            {formId == 1 ? (
              <EbsForm definition={cimmytDefinition} onSubmit={onSubmitCimmyt}>
                <DialogActions>
                  <Button type="submit" variant="contained" color="primary">
                    <Typography variant="body1">
                      <FormattedMessage
                        id="something.id"
                        defaultMessage="Next"
                      />
                    </Typography>
                  </Button>
                </DialogActions>
              </EbsForm>
            ) : (
              <EbsForm definition={irriDefinition} onSubmit={onSubmitCimmyt}>
                {/* <DialogActions> */}
                <Button type="submit" variant="contained" color="primary">
                  <Typography variant="body1">
                    <FormattedMessage id="something.id" defaultMessage="Next" />
                  </Typography>
                </Button>
                {/* </DialogActions> */}
              </EbsForm>
            )}
          </div>
        </Grid>
      </Grid>
    </div>
  )
})
// Type and required properties
GeneralFormAtom.propTypes = {}
// Default properties
GeneralFormAtom.defaultProps = {}

export default GeneralFormAtom
