import React, { useState } from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS
import { Grid, Button, DialogActions, Typography,
  Table,
  makeStyles,
  TableCell,
  TableBody,
  TableRow} from '@material-ui/core'
import EbsForm from 'ebs-form'
import { FormattedMessage } from 'react-intl'
import { useQuery } from '@apollo/client'
import { SMgraph } from 'utils/apollo'

import {
  FIND_TYSSUE_TYPE_LIST,
  FIND_PROGRAM_LIST,
  FIND_SERVICE_PROVIDER_LIST,
  FIND_PURPOSES_BY_SERVICE_TYPE_ID,
  FIND_SERVICE_TYPE_BY_SERVICE_PROVIDER_ID,
  FIND_SERVICE_BY_PURPOSE_ID,
} from 'utils/apollo/gql/RequestManager'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/

const useStyles = makeStyles((theme) => ({
  bold: {
    fontWeight: 600,
  },
  tableCell: {
    borderBottom: 'none',
    marginBottom: '0px',
    padding: '12px'
  },
  inlineStyle: {
    display: 'inline-block',
  },
  labelStyle: {
    fontWeight: 600,
    marginRight: '30px',
  },
  table1: {
    maxWidth: 500,
  },
  table2: {
    maxWidth: 500,
  },
}))

const ServiceProviderFormAtom = React.forwardRef((props, ref) => {
  const {
    serviceProviderForm,
    serviceProvider,
    value,
    handleChange,
    ...rest
  } = props

  const [programs, setPrograms] = React.useState([])
  const [serviceProviders, setServiceProviders] = React.useState([])
  const [serviceTypeDisabled, setServiceTypeDisabled] = React.useState(true)
  const [serviceTypes, setServiceTypes] = React.useState([])
  const [purposesDisabled, setPurposesDisabled] = React.useState(true)
  const [purposes, setPurposes] = React.useState([])
  const [serviceDisabled, setServiceDisabled] = React.useState(true)
  const [services, setServices] = React.useState([])
  const [nextDisabled, setNextDisabled] = React.useState(true)

  const tyssueTypesData = useQuery(FIND_TYSSUE_TYPE_LIST)
  const programsData = useQuery(FIND_PROGRAM_LIST)
  const serviceProvidersData = useQuery(FIND_SERVICE_PROVIDER_LIST)
  const serviceTypesByServiceProviderId = useQuery(FIND_SERVICE_PROVIDER_LIST)

 
  if (programs.length < 1 && programsData.data) {
    let newPrograms = []
    programsData.data.findProgramList.content.map((program) => {
      newPrograms.push({ value: program.id, label: program.programName })
    })
    setPrograms(newPrograms)
  }

  if (serviceProviders.length < 1 && serviceProvidersData.data) {
    let newServiceProviders = []
    serviceProvidersData.data.findServiceProviderList.content.map(
      (ServiceProvider) => {
        newServiceProviders.push({
          value: ServiceProvider.id,
          label: ServiceProvider.name,
        })
      }
    )
    setServiceProviders(newServiceProviders)
  }

  const brandColor = '#808080'

  const customStyles = {
    control: (base, state) => ({
      ...base,
      boxShadow: state.isFocused ? 0 : 0,
      borderColor: state.isFocused ? brandColor : base.borderColor,
      color: state.isDisabled ? base.borderColor : brandColor,
      '&:hover': {
        borderColor: state.isFocused ? brandColor : base.borderColor,
      },
      '&:disabled': {
        color: brandColor,
      },
    }),
  }

  //updates service {provider}
  const updateServiceTypes = (item, setValue) => {
    serviceProviderForm.serviceprovider = item
    setNextDisabled(true)

    // if (item.label.includes('CIMMYT')) {
    //   setFormId(1)
    // } else {
    //   setFormId(2)
    // }

    //variable is selected serviceProvider
    const newServiceTypes = []
    SMgraph
      .query({
        query: FIND_SERVICE_TYPE_BY_SERVICE_PROVIDER_ID,
        variables: {
          id: item.value,
        },
        fetchPolicy: 'no-cache',
      })
      .then(({ data }) => {
        // Populating the values of the Grid
        data.findServiceProvider.servicetypes.map((row) => {
          newServiceTypes.push({
            value: row.id,
            label: row.name,
          })
        })

        setServiceTypes(newServiceTypes)
        if (newServiceTypes.length == 1) {
          setValue('servicetype', newServiceTypes[0])
          updatePurposes(newServiceTypes[0], setValue)
        } else {
          setValue('servicetype', [])
        }
      })
      .catch((err) => console.log(err))
    setServiceTypeDisabled(false)
    setValue('purpose', [])
    setPurposesDisabled(true)
    setValue('service', [])
    setServiceDisabled(true)
    
  }

  // Filter to service type
  // const serviceTypeFilter = (item, getValues) => {
  //   let serviceTypes = [];
  //   const serviceProvider = serviceProvidersData.data.findServiceProviderList.content.filter(
  //     (sp) => sp.id === item.value
  //   );
  //   serviceProvider[0].servicetypes.map((item) => {
  //     // console.log(getValues('program').value)
  //       serviceTypes.push({ value: item.id, label: item.name, ...item });
  //   });
  //   setServiceTypes(serviceTypes);
  // };

  // Filter to Purpose
  const updatePurposes = (item, setValue) => {
    serviceProviderForm.serviceType = item

    //variable is selected serviceType
    setPurposesDisabled(false)
    setNextDisabled(true)
    const newPurposes = []
    SMgraph
      .query({
        query: FIND_PURPOSES_BY_SERVICE_TYPE_ID,
        variables: {
          id: item.value,
        },
        fetchPolicy: 'no-cache',
      })
      .then(({ data }) => {
        data.findServiceType.purposes.map((row) => {
          newPurposes.push({
            value: row.id,
            label: row.name,
          })
        })

        setPurposes(newPurposes)
        if (newPurposes.length == 1) {
          setValue('purpose', newPurposes[0])
          updateServices(newPurposes[0], setValue)
        } else {
          setValue('purpose', [])
        }
      })
      .catch((err) => console.log(err))

    setValue('service', [])
    setServiceDisabled(true)
  }

  // const purposeFilter = (item) => {
  //   let purposes = [];
  //   const servicesTypes = serviceTypes.filter((st) => st.id === item.value);
  //   servicesTypes.map((item) => {
  //     purposes.push({ value: item.id, label: item.name, ...item });
  //   });
  //   setPurposes(purposes);
  // };

  // Filter to Services
  const updateServices = (item, setValue) => {
    serviceProviderForm.purpose = item

    //variable is selected serviceType
    setServiceDisabled(false)
    setNextDisabled(true)
    const newServices = []
    SMgraph
      .query({
        query: FIND_SERVICE_BY_PURPOSE_ID,
        variables: {
          id: item.value,
        },
        fetchPolicy: 'no-cache',
      })
      .then(({ data }) => {
        data.findPurpose.services.map((row) => {
          newServices.push({
            value: row.id,
            label: row.name,
          })
        })

        setServices(newServices)
        if (newServices.length == 1) {
          setValue('service', newServices[0])
          updateTissueTypes(newServices[0], setValue)
        } else {
          setValue('service', [])
        }
      })
      .catch((err) => console.log(err))

    setValue('tissuetype', [])
  }

  const updateTissueTypes = (item, setValue) => {
    serviceProviderForm.service = item

     setNextDisabled(false)
  }

  const serviceProviderDefinition = (props) => {
    const { getValues, setValue } = props
    return {
      name: 'serviceProvider',
      components: [
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'Select',
          name: 'serviceprovider',
          options: serviceProviders,
          onChange: (element) => updateServiceTypes(element, setValue),
          inputProps: {
            placeholder: 'Service Provider',
            styles: customStyles,
          },
          defaultValue: serviceProviderForm?.serviceprovider,
          helper: {
            title:
              "Internal service unit that triage, and/or process clients' service requests.",
            placement: 'right',
            arrow: true,
          },
          rules: {
            required: 'Service Provider is required. Please select one.',
          },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'Select',
          name: 'servicetype',
          options: serviceTypes,
          onChange: (element) => updatePurposes(element, setValue),
          inputProps: {
            placeholder: 'Service Type',
            isDisabled: serviceTypeDisabled,
            styles: customStyles,
          },
          defaultValue: serviceProviderForm?.serviceType,
          helper: {
            title:
              'Type of services, such as Genotyping (G), Phytosanitary (P), seed quality (G), pathology including MLN screening',
            placement: 'right',
            arrow: true,
          },
          rules: { required: 'Service Type is required. Please select one.' },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'Select',
          name: 'purpose',
          options: purposes,
          onChange: (element) => updateServices(element, setValue),
          inputProps: {
            placeholder: 'Purpose',
            isDisabled: purposesDisabled,
            styles: customStyles,
          },
          defaultValue: serviceProviderForm?.purpose,
          helper: {
            title:
              'High level buiness needs that the service to address. For genotyping, values include MAS, QC, GS/GWAS, germplasm characterization',
            placement: 'right',
            arrow: true,
          },
          rules: { required: 'Purpose is required. Please select one.' },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'Select',
          name: 'service',
          options: services,
          onChange: (element) => updateTissueTypes(element, setValue),
          inputProps: {
            placeholder: 'Please select a service',
            isDisabled: serviceDisabled,
            styles: customStyles,
          },
          defaultValue: serviceProviderForm?.service,
          helper: {
            title:
              'Service or product name, corresponding to a specific vendor catalog product or service identifier',
            placement: 'right',
            arrow: true,
          },
          rules: { required: 'Service is required. Please select one.' },
        },
       
      ],
    }
  }

  const classes = useStyles()

  return (
    /*
     @prop data-testid: Id to use inside serviceproviderform.test.js file.
     */
    <div data-testid={'ServiceProviderFormTestId'} ref={ref}>
      <Typography variant="body2">
            <FormattedMessage
              id={'some.id'}
              defaultMessage={'Please provide all of the information below'}
            />
          </Typography>
          <br />
      <Grid container
      >
        <Grid item xs={2}>
        <Table className={classes.table1} size="medium">
        <TableBody>
            <TableRow>
              <TableCell align="left" className={classes.tableCell}>
                Service Provider
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell align="left" className={classes.tableCell}>
              Service Type
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell  align="left" className={classes.tableCell}>
              Purpose
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell align="left" className={classes.tableCell}>
              Service
              </TableCell>
            </TableRow>
        </TableBody>
      </Table>
        </Grid>
        <Grid item xs={7}>
          <EbsForm definition={serviceProviderDefinition}>
          { !nextDisabled && (
              <DialogActions>
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={() => handleChange(value + 1)}
                  >
                    <FormattedMessage id={'some.id'} defaultMessage={'NEXT'} />
                  </Button>
              </DialogActions>
            )}
          </EbsForm>
        </Grid>
        
      </Grid>
    </div>
  )
})
// Type and required properties
ServiceProviderFormAtom.propTypes = {}
// Default properties
ServiceProviderFormAtom.defaultProps = {}

export default ServiceProviderFormAtom
