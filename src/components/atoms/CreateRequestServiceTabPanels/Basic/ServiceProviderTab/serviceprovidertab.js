import React from 'react'
import PropTypes from 'prop-types'
import { Container, Grid } from '@material-ui/core'

import ServiceProviderForm from 'components/atoms/ServiceProviderForm'
// CORE COMPONENTS

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ServiceProviderTabAtom = React.forwardRef((props, ref) => {
  // const { children, ...rest } = props
  const {
    value,
    serviceProviderForm,
    handleChange,
    serviceProvider,
    onSubmitCimmyt,
    cimmyt,
    irri,
    ...rest
  } = props

  return (
    /*
     @prop data-testid: Id to use inside ServiceProviderTab.test.js file.
     */
    <div>
      <ServiceProviderForm
        serviceProviderForm={serviceProviderForm}
        value={value}
        handleChange={handleChange}
        serviceProvider={serviceProvider}
        onSubmitCimmyt={onSubmitCimmyt}
        cimmyt={cimmyt}
        irri={irri}
      />
    </div>
  )
})
// Type and required properties
ServiceProviderTabAtom.propTypes = {}
// Default properties
ServiceProviderTabAtom.defaultProps = {}

export default ServiceProviderTabAtom
