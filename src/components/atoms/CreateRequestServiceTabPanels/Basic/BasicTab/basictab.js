import React from 'react'
import PropTypes from 'prop-types'
import { Container, Grid } from '@material-ui/core'

import GeneralForm from 'components/atoms/GeneralForm'
// CORE COMPONENTS

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const BasicTabAtom = React.forwardRef((props, ref) => {
  // const { children, ...rest } = props
  const {
    value,
    general,
    serviceProviderForm,
    basicInformationOfServiceForm,
    handleChange,
    serviceProvider,
    onSubmitCimmyt,
    cimmyt,
    irri,
    requester,
    setRequester,
    requesterEmail,
    ...rest
  } = props

  return (
    /*
     @prop data-testid: Id to use inside basictab.test.js file.
     */
    <div>
     <GeneralForm general={general}
          cimmyt={cimmyt}
          requester={requester}
          setRequester={setRequester}
          requesterEmail={requesterEmail}
          value={value}
          onSubmitCimmyt={onSubmitCimmyt}
          handleChange={handleChange}
          serviceProviderForm={serviceProviderForm}
          basicInformationOfServiceForm={basicInformationOfServiceForm}/>
    </div>
  )
})
// Type and required properties
BasicTabAtom.propTypes = {}
// Default properties
BasicTabAtom.defaultProps = {}

export default BasicTabAtom
