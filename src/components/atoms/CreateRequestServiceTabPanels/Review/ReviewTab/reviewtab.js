import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS

import {
  Divider,
  Grid,
  makeStyles,
  Table,
  TableCell,
  TableBody,
  TableRow,
  Typography,
} from '@material-ui/core'

import { FormattedMessage } from 'react-intl'

const useStyles = makeStyles((theme) => ({
  bold: {
    fontWeight: 600,
  },
  tableCell: {
    borderBottom: 'noasdne',
  },
  inlineStyle: {
    display: 'inline-block',
  },
  labelStyle: {
    fontWeight: 600,
    marginRight: '30px',
  },
  mainLabel: {
    marginRight: '5px',
    fontWeight: 600,
    color: theme.palette.primary.dark,
  },
  notSetLabel: {
    color: theme.palette.primary.dark,
    fontStyle: 'italic',
  },
  table1: {
    maxWidth: 500,
  },
  table2: {
    maxWidth: 500,
  },
}))

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ReviewTabAtom = React.forwardRef((props, ref) => {
  const classes = useStyles()

  const {
    children,
    basicTabData1,
    basicTabData2,
    listDbId,
    listName,
    requestCode,
    ...rest
  } = props // bring back request code

  const { serviceprovider, serviceType, purpose, service } = basicTabData1

  const provider = serviceprovider?.label.includes('CIMMYT') ? 'CIMMYT' : 'IRRI'

  const dateOptions = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  }

  // remove all the value dups and uncomment the original value key

  const SERVICE_ORDER_FORM_VALUES = [
    {
      id: 'some.id',
      defaultMessage: 'Selected Service Provider',
      value: checkEmptyValue(serviceprovider?.label),
    },
    {
      id: 'some.id',
      defaultMessage: 'Selected Service Type',
      value: checkEmptyValue(serviceType?.label),
    },
    {
      id: 'some.id',
      defaultMessage: 'Selected Purpose',
      value: checkEmptyValue(purpose?.label),
    },
    {
      id: 'some.id',
      defaultMessage: 'Selected Service',
      value: checkEmptyValue(service?.label),
    },
  ]

  const CIMMYT_SERVICE_INFORMATION = [
    {
      id: 'some.id',
      defaultMessage: 'Program',
      value: checkEmptyValue(basicTabData2?.program),
    },
    {
      id: 'some.id',
      defaultMessage: 'Crop',
      value: checkEmptyValue(basicTabData2?.crop),
    },
    {
      id: 'some.id',
      defaultMessage: 'Requester',
      value: checkEmptyValue(basicTabData2?.requester),
    },
    {
      id: 'some.id',
      defaultMessage: 'Requester Email',
      value: checkEmptyValue(basicTabData2?.requesterEmail),
    },
    {
      id: 'some.id',
      defaultMessage: 'Tissue Type',
      value: checkEmptyValue(basicTabData2?.tissueType),
    },
    {
      id: 'some.id',
      defaultMessage: 'Admin Contact',
      value: checkEmptyValue(basicTabData2?.adminContactName),
    },
    {
      id: 'some.id',
      defaultMessage: 'Admin Email',
      value: checkEmptyValue(basicTabData2?.adminContactEmail),
    },
    {
      id: 'some.id',
      defaultMessage: 'ICC',
      value: checkEmptyValue(basicTabData2?.icc),
    },
    {
      id: 'some.id',
      defaultMessage: 'Submission Date',
      value: checkEmptyValue(
        basicTabData2?.completedBy?.toLocaleDateString('en-US', dateOptions)
      ),
    },
    {
      id: 'some.id',
      defaultMessage: 'Completed By',
      value: checkEmptyValue(
        basicTabData2?.submitionDate?.toLocaleDateString('en-US', dateOptions)
      ),
    },
    {
      id: 'some.id',
      defaultMessage: 'Description',
      value: checkEmptyValue(basicTabData2?.description),
    },
  ]

  const IRRI_SERVICE_INFORMATION = [
    {
      id: 'some.id',
      defaultMessage: 'Researcher',
      value: checkEmptyValue(basicTabData2?.researcher),
    },
    {
      id: 'some.id',
      defaultMessage: 'OCS Number',
      value: checkEmptyValue(basicTabData2?.ocsnumber),
    },
    {
      id: 'some.id',
      defaultMessage: 'Completed By',
      value: checkEmptyValue(
        basicTabData2?.completedBy?.toLocaleDateString('en-US', dateOptions)
      ),
    },
    {
      id: 'some.id',
      defaultMessage: 'Contact Person',
      value: checkEmptyValue(basicTabData2?.contactperson),
    },
    {
      id: 'some.id',
      defaultMessage: 'Description',
      value: checkEmptyValue(basicTabData2?.description),
    },
  ]

  function checkEmptyValue(value) {
    if (!value)
      return (
        <span className={classes.notSetLabel}>
          <FormattedMessage id={'some.id'} defaultMessage={'(Not Set)'} />
        </span>
      )
    return value
  }

  return (
    /*
     @prop data-testid: Id to use inside reviewtab.test.js file.
     */
    <>
      <br />
      <Typography>
        <FormattedMessage
          id={'some.id'}
          defaultMessage={
            'Please review the following information before submitting the request.'
          }
        />
      </Typography>

      <Divider />

      <br />

      <Typography>
        <span className={`${classes.mainLabel}`}>
          <FormattedMessage id={'some.id'} defaultMessage={'Service Order:'} />
        </span>
        <span>{checkEmptyValue(requestCode)}</span>
      </Typography>

      <Typography>
        <span className={`${classes.mainLabel}`}>
          <FormattedMessage id={'some.id'} defaultMessage={'Selected List:'} />
        </span>
        <span>{checkEmptyValue(listName)}</span>
      </Typography>

      <br />

      <Grid container spacing={3}>
        <Grid item xs={12} md={6}>
          <Typography>
            <span className={`${classes.mainLabel}`}>
              <FormattedMessage
                id={'some.id'}
                defaultMessage={'Selected Basic Information'}
              />
            </span>
          </Typography>

          <Table className={classes.table2} size="small">
            {provider === 'CIMMYT' ? (
              <TableBody>
                {CIMMYT_SERVICE_INFORMATION.map((row) => (
                  <TableRow key={row.defaultMessage}>
                    <TableCell
                      className={`${classes.tableCell} ${classes.bold}`}
                    >
                      <FormattedMessage
                        id={row.id}
                        defaultMessage={row.defaultMessage}
                      />
                    </TableCell>
                    <TableCell className={classes.tableCell}>
                      {row.value}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            ) : (
              <TableBody>
                {IRRI_SERVICE_INFORMATION.map((row) => (
                  <TableRow key={row.defaultMessage}>
                    <TableCell
                      className={`${classes.tableCell} ${classes.bold}`}
                    >
                      <FormattedMessage
                        id={row.id}
                        defaultMessage={row.defaultMessage}
                      />
                    </TableCell>
                    <TableCell className={classes.tableCell}>
                      {row.value}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            )}
          </Table>
        </Grid>

        <Grid item xs={12} md={6}>
          <Typography>
            <span className={`${classes.mainLabel}`}>
              <FormattedMessage
                id={'some.id'}
                defaultMessage={'Selected Service Information'}
              />
            </span>
          </Typography>

          <Table className={classes.table1} size="small">
            <TableBody>
              {SERVICE_ORDER_FORM_VALUES.map((row) => (
                <TableRow key={row.defaultMessage}>
                  <TableCell className={`${classes.tableCell} ${classes.bold}`}>
                    <FormattedMessage
                      id={row.id}
                      defaultMessage={row.defaultMessage}
                    />
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {row.value}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Grid>
      </Grid>
    </>
  )
})
// Type and required properties
ReviewTabAtom.propTypes = {}
// Default properties
ReviewTabAtom.defaultProps = {}

export default ReviewTabAtom
