import React, { useState, useEffect } from 'react';
import EbsGrid from 'ebs-grid-lib';

// CORE COMPONENTS
import SelectListRow from 'components/atoms/SelectListRow';
import ViewEntryList from 'components/molecules/ViewEntryListButton';

import { Checkbox, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import { B4Rrest } from 'utils/axios';
import { FormattedMessage } from 'react-intl';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/

const columns = [
  {
    Header: 'Id',
    accessor: 'listDbId',
    hidden: true,
  },
  // {
  //   Header: <FormattedMessage id={'some.id'} defaultMessage={'User'} />,
  //   accessor: 'personName',
  //   filter: true,
  // },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Abbrev'} />,
    accessor: 'abbrev',
    filter: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Name'} />,
    accessor: 'name',
    filter: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Display Name'} />,
    accessor: 'displayName',
    filter: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Type'} />,
    accessor: 'type',
    filter: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Description'} />,
    accessor: 'description',
    filter: true,
  },
];

const useStyles = makeStyles(() => ({
  rowActionStyle: {
    display: 'flex',
    alignItems: 'stretch',
  },
}));

const EntryListTabAtom = React.forwardRef((props, ref) => {
  const classes = useStyles();
  const { getEntryTabData } = props;
  const [data, setData] = useState(null);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(null);
  const [totalElements, setTotalElements] = useState(null);
  const [creatorDbId, setCreatorDbId] = useState(null);

  const email = 'bims.irri@gmail.com'; // TODO: remove this. This will be retrieve using the CS

  useEffect(() => {
    getUserProfile(email);
  }, []);

  async function getUserProfile(email) {
    /**
     * This for getting the creatorDbId of the logged in user using
     * the email. creatorDbId then will be used to get the lists
     * under the logged in user.
     */
    const body = {
      fields:
        'person.email | person.username | person.first_name AS firstName | person.last_name AS lastName | person.person_name AS personName | person.creator_id AS creatorDbId',
      email: `equals ${email}`,
    };

    const response = await B4Rrest.post('/persons-search', body);
    if (response) {
      setCreatorDbId(response.data.result.data[0].creatorDbId);
    }
  }

  async function fetch(page, pageSize, columnsToFilter, value) {
    let body = {
      fields:
        'list.id AS listDbId | list.abbrev | list.name | list.type | list.description | list.remarks | list.status',
      creatorDbId: creatorDbId,
    };
    if (columnsToFilter && value) {
      body = {
        fields:
          'list.id AS listDbId | list.abbrev | list.name | list.type | list.description | list.remarks | list.status',
        creatorDbId: creatorDbId,
        [columnsToFilter]: `equals %${value}%`,
      };
    }
    B4Rrest.post('/lists-search', body).then((response) => {
      const data = response.data.result.data;
      // TODO: find the type 'plot'. Currently there's none.
      const plotTypeData = data.filter((_data) => _data.type === 'germplasm');
      setData(plotTypeData);
      setPage(page);
      setTotalPages(plotTypeData.length);
    });
  }

  /*
        @rowData: Object with row data.
        @refresh: Function to refresh Grid data.
        */
  const rowActions = (rowData, refresh) => {
    return (
      <div className={classes.rowActionStyle}>
        <SelectListRow rowData={rowData} getEntryTabData={getEntryTabData} />
        <ViewEntryList rowData={rowData} />
      </div>
    );
  };

  return (
    /*
     @prop data-testid: Id to use inside entrylisttab.test.js file.
     */
    <>
      <Typography variant='body2'>
        <FormattedMessage
          id={'some.id'}
          defaultMessage={'This is the browser for list of plot-type lists.'}
        />
      </Typography>
      <br />
      <EbsGrid
        callstandard='brapi'
        columns={columns}
        data={data}
        fetch={fetch}
        globalfilter
        indexing
        page={page}
        pagination
        rowactions={rowActions}
        toolbar={true}
        totalPages={totalPages}
        totalElements={totalElements}
      />
    </>
  );
});
// Type and required properties
EntryListTabAtom.propTypes = {};
// Default properties
EntryListTabAtom.defaultProps = {};

export default EntryListTabAtom;
