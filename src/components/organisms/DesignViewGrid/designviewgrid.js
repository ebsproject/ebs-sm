import React, { useState } from 'react';
import { Box, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import EbsGrid from 'ebs-grid-lib';
import StatusActionDesign from 'components/molecules/StatusActionDesign';
import {
  getLaboratoryReport,
  saveDesignBatchByID,
  getDesignListBatch,
} from 'utils/other/SampleManager';

const useStyles = makeStyles((theme) => ({
  container: {
    marginTop: '100px',
  },
  rowActionStyle: {
    display: 'flex',
    alignItems: 'stretch',
  },
}));

const DesignViewGrid = React.forwardRef((props, ref) => {
  const [data, setData] = useState(null);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(null);
  const [totalElements, setTotalElements] = useState(null);
  const classes = useStyles();
  // Columns
  const columns = [
    {
      Header: 'Id',
      accessor: 'id',
      hidden: true,
    },
    {
      Header: 'Batch Name',
      accessor: 'Name',
      filter: true,
    },
    {
      Header: 'Description',
      accessor: 'Objective',
      filter: true,
    },
    {
      Header: 'Total',
      accessor: 'Total',
      filter: true,
    },
    {
      Header: 'Total # Plates',
      accessor: 'num_containers',
      filter: true,
    },
    {
      Header: 'First Plate Name',
      accessor: 'first_Plate',
      filter: true,
    },
    {
      Header: 'Last Plate Name',
      accessor: 'last_plate_number',
      filter: true,
    },
    {
      Header: 'Status Update Date (Date)',
      accessor: 'updateDate',
      filter: true,
    },
    {
      Header: 'Status Update by (Name)',
      accessor: 'updateBy',
      filter: true,
    },
  ];

  const notSetLabel = (
    <Typography component='span' color='primary'>
      <Box fontStyle='italic'>(not set)</Box>
    </Typography>
  );

  function tableValue(value) {
    if (value === '' || value === null) return notSetLabel;
    return value;
  }

  async function fetch(page, pageSize, columnsToFilter, value) {
    // Update the data of the Grid based on user interaction with the Grid
    let filters = [];
    // Getting all the filters before passing to the query
    if (columnsToFilter && columnsToFilter.length > 0) {
      columnsToFilter.map((_column) => {
        // This is for catching the filter for the crop that is "crop.cropName"
        let column = '';
        let _mod = 'LK';
        switch (_column) {
          case 'Total':
            _mod = 'EQ';
            column = 'numMembers';
            break;
          default:
            column = _column;
            break;
        }
        filters.push({
          mod: _mod,
          col: column,
          val: value,
        });
      });
    }
    filters.push({
      mod: 'EQ',
      col: 'inDesign',
      val: 'false',
    });

    let newData = [];
    getDesignListBatch(0).then((response) => {
      newData = response;
      setData(newData);
      setPage(page);
      setTotalPages();
    });
  }

  const toolbarActions = (dataSelection, refresh) => {};

  const rowActions = (rowData, refresh) => {
    return (
      <div>
        <StatusActionDesign
          row={rowData}
          actionDownload={getLaboratoryReport}
          actionSaveDesign={saveDesignBatchByID}
          refresh={refresh}
        />
      </div>
    );
  };

  /*
        @prop data-testid: Id to use inside grid.test.js file.
        */
  return (
    <div className={classes.container}>
      <EbsGrid
        toolbar={true}
        data-testid={'GridTestId'}
        title='Design'
        columns={columns}
        //toolbaractions={toolbarActions}
        rowactions={rowActions}
        fetch={fetch}
        data={data}
        page={page}
        totalPages={totalPages}
        totalElements={totalElements}
        globalfilter
        pagination
        indexing
      />
    </div>
  );
});

// Type and required properties
DesignViewGrid.propTypes = {};
// Default properties
DesignViewGrid.defaultProps = {};

export default DesignViewGrid;
