import React, { useState } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND MOLECULES TO USE
import EbsGrid from 'ebs-grid-lib';
import { Box, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { SMgraph } from 'utils/apollo';
import { FIND_REQUEST_LIST } from 'utils/apollo/gql/RequestManager';
import { FormattedMessage } from 'react-intl';
import { SelectColumnFilter } from 'utils/other/generalFunctions';
import AcceptRequestButton from 'components/molecules/AcceptButton';
import RejectRequestButton from 'components/molecules/RejectButton';
import CreateBatchButton from 'components/organisms/RequestBatch';
import CreateRequest from 'components/molecules/CreateRequestButton';
const useStyles = makeStyles((theme) => ({
  rowActionStyle: {
    display: 'flex',
    alignItems: 'stretch',
  },
}));

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const RequestReviewGridOrganism = React.forwardRef((props, ref) => {
  const [data, setData] = useState(null);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(null);
  const [totalElements, setTotalElements] = useState(1);
  const classes = useStyles();
  // Columns
  const columns = [
    {
      Header: 'Id',
      accessor: 'id',
      hidden: true,
    },
    {
      Header: (
        <FormattedMessage id={'sm.req.tbl.hdr.status'} defaultMessage={'Status'} />
      ),
      accessor: 'status',
      Filter: SelectColumnFilter,
      filter: 'include',
    },
    {
      Header: (
        <FormattedMessage id={'sm.req.tbl.hdr.srid'} defaultMessage={'Request'} />
      ),
      accessor: 'requestCode',
      filter: true,
    },
    {
      Header: (
        <FormattedMessage
          id={'sm.req.tbl.hdr.requester'}
          defaultMessage={'Requester'}
        />
      ),
      accessor: 'requester',
      filter: true,
    },
    {
      Header: (
        <FormattedMessage id={'sm.req.tbl.hdr.crop'} defaultMessage={'Crop'} />
      ),
      accessor: 'crop',
      filter: true,
    },
    {
      Header: (
        <FormattedMessage id={'sm.req.tbl.hdr.program'} defaultMessage={'Program'} />
      ),
      accessor: 'program',
      filter: true,
    },
    {
      Header: (
        <FormattedMessage id={'sm.req.tbl.hdr.icccode'} defaultMessage={'ICC'} />
      ),
      accessor: 'icc',
      filter: true,
    },
    {
      Header: (
        <FormattedMessage id={'sm.req.tbl.hdr.purpose'} defaultMessage={'Purpose'} />
      ),
      accessor: 'purpose',
      filter: true,
    },
    {
      Header: (
        <FormattedMessage id={'sm.req.tbl.hdr.service'} defaultMessage={'Service'} />
      ),
      accessor: 'service',
      filter: true,
    },
    {
      Header: (
        <FormattedMessage
          id={'sm.req.tbl.hdr.servprov'}
          defaultMessage={'Service Provider'}
        />
      ),
      accessor: 'serviceprovider',
      filter: true,
    },
    {
      Header: (
        <FormattedMessage id={'sm.req.tbl.hdr.quantity'} defaultMessage={'Total'} />
      ),
      accessor: 'totalEntities',
      filter: true,
    },
    {
      Header: (
        <FormattedMessage
          id={'sm.req.tbl.hdr.submitDate'}
          defaultMessage={'Submit Date'}
        />
      ),
      accessor: 'submitionDate',
      filter: true,
    },

    {
      Header: (
        <FormattedMessage
          id={'sm.req.tbl.hdr.complete'}
          defaultMessage={'Complete By'}
        />
      ),
      accessor: 'completedBy',
      filter: true,
    },

    {
      Header: (
        <FormattedMessage
          id={'sm.req.tbl.hdr.type'}
          defaultMessage={'Service Type'}
        />
      ),
      accessor: 'servicetype',
      filter: true,
    },
  ];

  const notSetLabel = (
    <Typography component='span' color='primary'>
      <Box fontStyle='italic'>(not set)</Box>
    </Typography>
  );

  function tableValue(value) {
    /**
     * This function is to check if the value of the row from the rowData
     * is a string or null. This is because a request can be created
     * in which some of its values can be an empty string or a
     * null value.
     */
    if (value === '' || value === null) return notSetLabel;
    return value;
  }

  async function fetch(page, pageSize, columnsToFilter, value) {
    // Update the data of the Grid based on user interaction with the Grid
    let filters = [];
    // Getting all the filters before passing to the query
    if (columnsToFilter && columnsToFilter.length > 0) {
      columnsToFilter.map((_column) => {
        // This is for catching the filter for the crop that is "crop.cropName"
        let column = '';
        let _mod = 'LK';
        switch (_column) {
          case 'crop':
            column = 'crop.cropName';
            break;
          case 'purpose':
            column = 'purpose.name';
            break;
          case 'serviceprovider':
            column = 'serviceprovider.name';
            break;
          case 'service':
            column = 'service.name';
            break;
          case 'servicetype':
            column = 'servicetype.name';
            break;
          case 'status':
            column = 'status.name';
            break;
          case 'program':
            column = 'program.programName';
            break;
          case 'totalEntities':
            _mod = 'EQ';
            column = _column;
            break;
          case 'submitionDate' || 'completedBy':
            _mod = 'EQ';
            column = _column;
            break;
          default:
            column = _column;
            break;
        }
        filters.push({
          mod: _mod,
          col: column,
          val: value,
        });
      });
    }
    SMgraph.query({
      query: FIND_REQUEST_LIST,
      variables: {
        page: page || 1,
        size: pageSize || 10,
        filters: filters,
        disjunctionFilters:
          columnsToFilter && columnsToFilter.length > 0 ? true : false,
      },
      fetchPolicy: 'no-cache',
    })
      .then(({ data }) => {
        // Populating the values of the Grid
        let newData = [];
        data.findRequestList.content.map((row) => {
          newData.push({
            ...row,
            requestCode: tableValue(row.requester),
            requester: tableValue(row.requester),
            status: tableValue(row.status.name),
            crop: tableValue(row.crop.cropName),
            purpose: tableValue(row.purpose.name),
            serviceprovider: tableValue(row.serviceprovider.name),
            service: tableValue(row.service.name),
            servicetype: tableValue(row.servicetype.name),
            program: tableValue(row.program.programName),
            programId: tableValue(row.program.id),
            serviceId: tableValue(row.service.id),
            purposeId: tableValue(row.purpose.id),
          });
        });
        setData(newData);
        setPage(page);
        setTotalPages(data.findRequestList.totalPages);
        setTotalElements(data.findRequestList.totalElements);
      })
      .catch((err) => console.log(err));
  }
  const toolbarActions = (dataSelection, refresh) => {
    let newStatuses = [];
    if (dataSelection.length > 1) {
      newStatuses = dataSelection.filter(
        (data) => data.original.status === 'Approved',
      );
    }

    return (
      <>
        <CreateBatchButton requests={newStatuses} refresh={refresh} />

        <CreateRequest refresh={fetch} />
      </>
    );
  };

  const rowActions = (rowData, refresh) => {
    return (
      <div className={classes.rowActionStyle}>
        <AcceptRequestButton rowData={rowData} refresh={refresh} idStatus={2} />
        <RejectRequestButton rowData={rowData} refresh={refresh} />
      </div>
    );
  };
  return (
    /*
     @prop data-testid: Id to use inside requestreviewgrid.test.js file.
     */
    <div ref={ref} data-testid={'RequestReviewGridId'}>
      <EbsGrid
        toolbar={true}
        title='Request List'
        columns={columns}
        toolbaractions={toolbarActions}
        rowactions={rowActions}
        fetch={fetch}
        select='multi'
        data={data}
        page={page}
        totalPages={totalPages}
        totalElements={totalElements}
        globalfilter
        pagination
        indexing
      />
    </div>
  );
});
// Type and required properties
RequestReviewGridOrganism.propTypes = {};
// Default properties
RequestReviewGridOrganism.defaultProps = {};

export default RequestReviewGridOrganism;
