import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { SMgraph } from 'utils/apollo';
import { IconButton } from '@material-ui/core';
import EbsGrid from 'ebs-grid-lib';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import GetAppIcon from '@material-ui/icons/GetApp';
import DescriptionIcon from '@material-ui/icons/Description';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { makeStyles } from '@material-ui/core';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';
import { FIND_VENDOR_LIST, FIND_REQUEST_LIST } from 'utils/apollo/gql/VendorOrder';

const useStyles = makeStyles((theme) => ({
  container: {
    marginTop: '100px',
  },
  rowActionStyle: {
    display: 'flex',
    alignItems: 'stretch',
  },
  CheckCircleOutlineStyle: {
    fill: '#4caf50',
  },
  HighlightOffOutlinedStyle: {
    fill: '#f44336',
  },
  ErrorOutlineOutlinedStyle: {
    fill: '#00bcd4',
  },
}));

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const VendorOrderOrganism = React.forwardRef((props, ref) => {
  const [data, setData] = useState(null);
  const [page, setPage] = useState(1);
  const [totalElements, setTotalElements] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const classes = useStyles();

  // Columns
  const columns = [
    { Header: 'ID', accessor: 'id', hidden: true },
    { Header: 'Status', accessor: 'status', filter: true },
    { Header: 'Vendor Order', accessor: 'vendorOrder', filter: true },
    {
      Header: 'Customer Project ID',
      accessor: 'customerProjectId',
      filter: true,
    },
    { Header: 'Vendor', accessor: 'vendor', filter: true },
    {
      Header: 'Date Plate Shipment',
      accessor: 'datePlateShipment',
      filter: true,
    },
    { Header: 'Plate', accessor: 'plate', filter: true },
    { Header: 'Samples', accessor: 'samples', filter: true },
    { Header: 'Markers', accessor: 'markers', filter: true },
    { Header: 'Comments', accessor: 'comments', filter: true },
  ];

  async function fetch(page, pageSize, columnsToFilter, value) {
    // console.log("fetch called");
    SMgraph.query({
      query: FIND_REQUEST_LIST,
      fetchPolicy: 'no-cache',
    })
      .then(({ data }) => {
        let newData = [];
        data.findRequestList.content.map((row) => {
          newData.push({
            ...row,
            vendor: row.serviceprovider.name,
            vendorOrder: row.description,
            status: row.status.name,
            customerProjectId: row.id,
            id: row.id,
            datePlateShipment: row.submitionDate,
            markers: row.totalEntities,
            samples: row.totalEntities,
            plate: row.tenant,
            comments: row.purpose.code,
          });
        });
        setTotalPages(data.findRequestList.totalPages);
        setTotalElements(data.findRequestList.totalElements);
        setData(newData);
      })
      .catch((err) => console.log(err));
  }

  /*
        @dataSelection: Array object with data rows selected.
        @refresh: Function to refresh Grid data.
        */
  const toolbarActions = (dataSelection, refresh) => {
    return (
      <div className={classes.rowActionStyle}>
        <IconButton
          title='title'
          onClick={() => {
            alert(dataSelection);
            refresh();
          }}
          color='inherit'
        >
          <AddCircleOutlineIcon />
        </IconButton>
        <IconButton
          title='Validate'
          onClick={() => {
            alert(dataSelection);
            refresh();
          }}
          color='inherit'
        >
          <EditOutlinedIcon />
        </IconButton>
        <IconButton
          title='Validate'
          onClick={() => {
            alert(dataSelection);
            refresh();
          }}
          color='inherit'
        >
          <DeleteOutlinedIcon />
        </IconButton>
      </div>
    );
  };

  /*
        @rowData: Object with row data.
        @refresh: Function to refresh Grid data.
        */
  const rowActions = (rowData, refresh) => {
    return (
      <div className={classes.rowActionStyle}>
        <IconButton
          title='Validate'
          onClick={() => {
            alert(JSON.stringify(rowData));
            refresh();
          }}
        >
          <EditIcon />
        </IconButton>
        <IconButton
          title='Validate'
          onClick={() => {
            alert(JSON.stringify(rowData));
            refresh();
          }}
        >
          <VisibilityIcon />
        </IconButton>
        <IconButton
          title='Validate'
          onClick={() => {
            alert(JSON.stringify(rowData));
            refresh();
          }}
        >
          <GetAppIcon />
        </IconButton>
        <IconButton
          title='Validate'
          onClick={() => {
            alert(JSON.stringify(rowData));
            refresh();
          }}
        >
          <DescriptionIcon />
        </IconButton>
        <IconButton
          title='Validate'
          onClick={() => {
            alert(JSON.stringify(rowData));
            refresh();
          }}
        >
          <DeleteForeverIcon />
        </IconButton>
      </div>
    );
  };

  return (
    /*
     @prop data-testid: Id to use inside vendororder.test.js file.
     */
    <div ref={ref} data-testid={'VendorOrderTestId'}>
      <EbsGrid
        fetch={fetch}
        select='multi'
        data={data}
        page={page}
        totalPages={totalPages}
        totalElements={totalElements}
        toolbar={true}
        title='Vendor'
        columns={columns}
        toolbaractions={toolbarActions}
        rowactions={rowActions}
        globalfilter
        pagination
        indexing
      />
    </div>
  );
});
// Type and required properties
VendorOrderOrganism.propTypes = {};
// Default properties
VendorOrderOrganism.defaultProps = {};

export default VendorOrderOrganism;
