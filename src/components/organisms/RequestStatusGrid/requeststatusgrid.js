import React, { useState } from 'react';
import PropTypes from 'prop-types';
import EbsGrid from 'ebs-grid-lib';
import { IconButton, Box, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { FormattedMessage } from 'react-intl';
import { SMgraph } from 'utils/apollo';
import { SelectColumnFilter } from 'utils/other/generalFunctions';
import { Edit } from '@material-ui/icons';
import GridHeaderAtom from 'components/atoms/GridHeader';
import CreateRequest from 'components/molecules/CreateRequestButton';
import ViewRequest from 'components/molecules/ViewRequestButton';
import DeleteRequest from 'components/molecules/DeleteRequestButton';
import {
  FIND_ENTRY_LIST_LIST,
  FIND_REQUEST_LIST,
} from 'utils/apollo/gql/RequestManager';

const useStyles = makeStyles((theme) => ({
  rowActionStyle: {
    display: 'flex',
    alignItems: 'stretch',
  },
}));
//MAIN FUNCTION
/*
    @param props: component properties.
    @param ref: reference made by React.forward.
    */

const RequestStatusGridOrganism = React.forwardRef((props, ref) => {
  const [data, setData] = useState(null);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(null);
  const [totalElements, setTotalElements] = useState(1);
  const classes = useStyles();
  // Columns
  const columns = [
    {
      Header: 'Id',
      accessor: 'id',
      hidden: true,
    },
    {
      Header: 'Status',
      accessor: 'status',
      Filter: SelectColumnFilter,
      filter: 'include',
    },
    {
      Header: 'Request',
      accessor: 'requestCode',
      filter: true,
    },
    {
      Header: 'Requester',
      accessor: 'requester',
      filter: true,
    },
    {
      Header: 'Crop',
      accessor: 'crop',
      filter: true,
    },
    {
      Header: 'Service Type',
      accessor: 'servicetype',
      filter: true,
    },
    {
      Header: 'Service Provider',
      accessor: 'serviceprovider',
      filter: true,
    },
    {
      Header: 'Services',
      accessor: 'service',
      filter: true,
    },
    {
      Header: 'Purpose',
      accessor: 'purpose',
      filter: true,
    },
    {
      Header: 'Total',
      accessor: 'totalEntities',
      filter: true,
    },
  ];

  const notSetLabel = (
    <Typography component='span' color='primary'>
      <Box fontStyle='italic'>(not set)</Box>
    </Typography>
  );

  function tableValue(value) {
    /**
     * This function is to check if the value of the row from the rowData
     * is a string or null. This is because a request can be created
     * in which some of its values can be an empty string or a
     * null value.
     */
    if (value === '' || value === null) return notSetLabel;
    return value;
  }

  async function fetch(page, pageSize, columnsToFilter, value) {
    // Update the data of the Grid based on user interaction with the Grid
    let filters = [];
    // Getting all the filters before passing to the query
    if (columnsToFilter && columnsToFilter.length > 0) {
      columnsToFilter.map((_column) => {
        // This is for catching the filter for the crop that is "crop.cropName"
        let column = '';
        if (_column === 'crop') column = 'crop.cropName';
        if (_column === 'purpose') column = 'purpose.name';
        if (_column === 'serviceprovider') column = 'serviceprovider.name';
        if (_column === 'service') column = 'service.name';
        if (_column === 'servicetype') column = 'servicetype.name';
        if (_column === 'status') column = 'status.name';

        filters.push({
          mod: 'LK',
          col: column,
          val: value,
        });
      });
    }

    SMgraph.query({
      query: FIND_REQUEST_LIST,
      variables: {
        page: page || 1,
        size: pageSize || 10,
        filters: filters,
        disjunctionFilters:
          columnsToFilter && columnsToFilter.length > 0 ? true : false,
      },
      fetchPolicy: 'no-cache',
    })
      .then(({ data }) => {
        // Populating the values of the Grid
        let newData = [];
        data.findRequestList.content.map((row) => {
          newData.push({
            ...row,
            requestCode: tableValue(row.requester),
            requestCode: tableValue(row.requestCode),
            status: tableValue(row.status.name),
            crop: tableValue(row.crop.cropName),
            purpose: tableValue(row.purpose.name),
            serviceprovider: tableValue(row.serviceprovider.name),
            service: tableValue(row.service.name),
            servicetype: tableValue(row.servicetype.name),
          });
        });
        setData(newData);
        setPage(page);
        setTotalPages(data.findRequestList.totalPages);
        setTotalElements(data.findRequestList.totalElements);
      })
      .catch((err) => console.log(err));
  }

  /*
        @dataSelection: Array object with data rows selected.
        @refresh: Function to refresh Grid data.
        */
  const toolbarActions = (dataSelection, refresh) => {
    /**
     * Only show Create Batch button if two or more rows are selected
     * with status of "Approved"
     */
    let newStatuses = [];
    if (dataSelection.length > 1) {
      newStatuses = dataSelection.filter(
        (data) => data.original.status === 'Approved',
      );
    }

    return (
      <>
        {/*

        <Button
          variant="contained"
          color="primary"
          disabled={newStatuses.length > 0 ? false : true}
        >
          <FormattedMessage
            id={"sm.btn.dialog.btn.lbl.create"}
            defaultMessage={"Create Batch"}
          />
        </Button>
      */}
        {/* <CreateRequest refresh={fetch} /> */}
      </>
    );
  };

  /*
        @rowData: Object with row data.
        @refresh: Function to refresh Grid data.
        */
  const rowActions = (rowData, refresh) => {
    return (
      <div className={classes.rowActionStyle}>
        <ViewRequest rowData={rowData} refresh={refresh} />
        <IconButton
          title='Edit'
          color='primary'
          onClick={() => {
            alert('EDIT');
            fetch();
          }}
        >
          <Edit />
        </IconButton>
        <DeleteRequest rowData={rowData} refresh={refresh} />
        {/*Remove multi-line comment to view Accept, Reject and Information icon buttons */}
        {/* <Grid
          container
          direction="row"
          justify="flex-start"
          alignItems="flex-start"
          spacing={3}
        >
          <Grid item xs={4} sm={4} md={4} lg={4} xl={4}>
            <AcceptButton rowData={rowData} refresh={refresh} />
          </Grid>
          <Grid item xs={4} sm={4} md={4} lg={4} xl={4}>
            <RejectButton rowData={rowData} refresh={refresh} />
          </Grid>
          <Grid item xs={4} sm={4} md={4} lg={4} xl={4}>
            <ValidateButton rowData={rowData} refresh={refresh} />
          </Grid>
        </Grid> */}
      </div>
    );
  };

  return (
    /*
     @prop data-testid: Id to use inside requeststatusgrid.test.js file.
     */
    <div ref={ref} data-testid={'RequestStatusGridTestId'}>
      <GridHeaderAtom
        title={'Request Manager'}
        description={
          'This is the browser for the Request Manager. You may access the requests here.'
        }
      />
      <EbsGrid
        toolbar={true}
        columns={columns}
        toolbaractions={toolbarActions}
        rowactions={rowActions}
        fetch={fetch}
        select='multi'
        data={data}
        page={page}
        totalElements={totalElements}
        totalPages={totalPages}
        globalfilter
        pagination
        indexing
      />
    </div>
  );
});
// Type and required properties
RequestStatusGridOrganism.propTypes = {};
// Default properties
RequestStatusGridOrganism.defaultProps = {};

export default RequestStatusGridOrganism;
