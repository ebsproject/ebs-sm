import React, { useState } from 'react';
import { Box, Typography } from '@material-ui/core';
import EbsGrid from 'ebs-grid-lib';
import { makeStyles } from '@material-ui/core/styles';
import { SMgraph } from 'utils/apollo';
import { FIND_BATCH_LIST } from 'utils/apollo/gql/RequestManager';
import AcceptBatchButton from 'components/molecules/AcceptBatchButton';
import RejectBatchButton from 'components/molecules/RejectBatchButton';

const useStyles = makeStyles((theme) => ({
  rowActionStyle: {
    display: 'flex',
    alignItems: 'stretch',
  },
}));

const BatchVIewGrid = React.forwardRef((props, ref) => {
  const [data, setData] = useState(null);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(null);
  const [totalElements, setTotalElements] = useState(null);
  const classes = useStyles();

  // Columns
  const columns = [
    {
      Header: 'Id',
      accessor: 'id',
      hidden: true,
    },
    {
      Header: 'Batch Name',
      accessor: 'name',
      filter: true,
    },
    {
      Header: 'Description',
      accessor: 'objective',
      filter: true,
    },
    {
      Header: 'Total',
      accessor: 'total',
      filter: true,
    },
  ];

  const notSetLabel = (
    <Typography component='span' color='primary'>
      <Box fontStyle='italic'>(not set)</Box>
    </Typography>
  );

  function tableValue(value) {
    if (value === '' || value === null) return notSetLabel;
    return value;
  }

  async function fetch(page, pageSize, columnsToFilter, value) {
    // Update the data of the Grid based on user interaction with the Grid
    let filters = [];
    // Getting all the filters before passing to the query
    if (columnsToFilter && columnsToFilter.length > 0) {
      columnsToFilter.map((_column) => {
        // This is for catching the filter for the crop that is "crop.cropName"
        let column = '';
        let _mod = 'LK';
        switch (_column) {
          case 'total':
            _mod = 'EQ';
            column = 'numMembers';
            break;
          default:
            column = _column;
            break;
        }
        filters.push({
          mod: _mod,
          col: column,
          val: value,
        });
      });
    }
    filters.push({
      mod: 'EQ',
      col: 'inDesign',
      val: 'false',
    });

    SMgraph.query({
      query: FIND_BATCH_LIST,
      variables: {
        page: page || 1,
        size: pageSize || 10,
        filters: filters,
      },
      fetchPolicy: 'no-cache',
    })
      .then(({ data }) => {
        // Populating the values of the Grid

        let newData = [];
        data.findBatchList.content.map((row) => {
          newData.push({
            ...row,
            total: tableValue(row.numMembers),
          });
        });
        setData(newData);
        setPage(page);
        setTotalPages(data.findBatchList.totalPages);
        setTotalElements(data.findBatchList.totalElements);
      })
      .catch((err) => console.log(err));
  }

  /*
        @dataSelection: Array object with data rows selected.
        @refresh: Function to refresh Grid data.
        */
  const toolbarActions = (dataSelection, refresh) => {};

  /*
        @rowData: Object with row data.
        @refresh: Function to refresh Grid data.
        */
  const rowActions = (rowData, refresh) => {
    return (
      <div className={classes.rowActionStyle}>
        <AcceptBatchButton rowData={rowData} refresh={refresh} idStatus={5} />
        <RejectBatchButton rowData={rowData} refresh={refresh} idStatus={3} />
      </div>
    );
  };

  /*
        @prop data-testid: Id to use inside grid.test.js file.
        */
  return (
    <div ref={ref} data-testid={'BatchViewGridId'}>
      <EbsGrid
        toolbar={true}
        data-testid={'GridTestId'}
        title='Batch'
        columns={columns}
        //toolbaractions={toolbarActions}
        rowactions={rowActions}
        fetch={fetch}
        select='multi'
        data={data}
        page={page}
        totalPages={totalPages}
        totalElements={totalElements}
        globalfilter
        pagination
        indexing
      />
    </div>
  );
});

// Type and required properties
BatchVIewGrid.propTypes = {};
// Default properties
BatchVIewGrid.defaultProps = {};

export default BatchVIewGrid;
