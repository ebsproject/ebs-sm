import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import Plate96Wells from './plate96wells'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

const zeroPad = (num, places) => String(num).padStart(places, '0');

function createData(col0, col1, col2, col3, col4, col5, col6, col7
  , col8, col9, col10, col11, col12) {
  
  return {col0,  col1, col2, col3, col4, col5, col6, col7
    , col8, col9, col10, col11, col12};
}
const createDataBaseColumns = (plateName) =>{

  let places =5;
  let plateformat = plateName ;
  let rows = [
      createData('A', plateformat+zeroPad(1,places), 
                      plateformat+zeroPad(9,places), 
                      plateformat+zeroPad(17,places), 
                      plateformat+zeroPad(25,places) , 
                      plateformat+zeroPad(33,places) ,
                      plateformat+zeroPad(41,places), 
                      plateformat+zeroPad(50,places),
                      plateformat+zeroPad(58,places), 
                      plateformat+zeroPad(66,places), 
                      plateformat+zeroPad(74,places), 
                      plateformat+zeroPad(82,places), 
                      plateformat+zeroPad(91,places)),

      createData('B', plateformat+zeroPad(2,places), 
                      plateformat+zeroPad(10,places), 
                      plateformat+zeroPad(18,places), 
                      plateformat+zeroPad(26,places) , 
                      plateformat+zeroPad(34,places) ,
                      plateformat+zeroPad(44,places), 
                      plateformat+zeroPad(51,places),
                      plateformat+zeroPad(59,places), 
                      plateformat+zeroPad(67,places), 
                      plateformat+zeroPad(75,places), 
                      plateformat+zeroPad(83,places), 
                      plateformat+zeroPad(92,places)),

      createData('C', plateformat+zeroPad(3,places), 
                      plateformat+zeroPad(11,places), 
                      plateformat+zeroPad(19,places), 
                      plateformat+zeroPad(27,places) , 
                      plateformat+zeroPad(35,places) ,
                      plateformat+zeroPad(43,places), 
                      plateformat+zeroPad(52,places),
                      plateformat+zeroPad(60,places), 
                      plateformat+zeroPad(68,places), 
                      plateformat+zeroPad(76,places), 
                      plateformat+zeroPad(84,places), 
                      plateformat+zeroPad(93,places)),
      
      createData('D', plateformat+zeroPad(4,places), 
                      plateformat+zeroPad(12,places), 
                      plateformat+zeroPad(20,places), 
                      plateformat+zeroPad(28,places) , 
                      plateformat+zeroPad(36,places) ,
                      plateformat+zeroPad(44,places), 
                      plateformat+zeroPad(53,places),
                      plateformat+zeroPad(61,places), 
                      plateformat+zeroPad(69,places), 
                      plateformat+zeroPad(77,places), 
                      plateformat+zeroPad(85,places), 
                      plateformat+zeroPad(94,places)),
      
      createData('E', plateformat+zeroPad(5,places), 
                      plateformat+zeroPad(13,places), 
                      plateformat+zeroPad(21,places), 
                      plateformat+zeroPad(29,places) , 
                      plateformat+zeroPad(37,places) ,
                      plateformat+zeroPad(45,places), 
                      plateformat+zeroPad(54,places),
                      plateformat+zeroPad(62,places), 
                      plateformat+zeroPad(70,places), 
                      plateformat+zeroPad(78,places), 
                      plateformat+zeroPad(86,places), 
                      plateformat+zeroPad(95,places)),
                      
      createData('F', plateformat+zeroPad(6,places), 
                      plateformat+zeroPad(14,places), 
                      plateformat+zeroPad(22,places), 
                      plateformat+zeroPad(30,places) , 
                      plateformat+zeroPad(38,places) ,
                      plateformat+zeroPad(46,places), 
                      plateformat+zeroPad(55,places),
                      plateformat+zeroPad(63,places), 
                      plateformat+zeroPad(71,places), 
                      plateformat+zeroPad(79,places), 
                      plateformat+zeroPad(87,places), 
                      plateformat+zeroPad(96,places)),
    
      createData('G', plateformat+zeroPad(7,places), 
                      plateformat+zeroPad(15,places), 
                      plateformat+zeroPad(23,places), 
                      plateformat+zeroPad(31,places) , 
                      plateformat+zeroPad(39,places) ,
                      plateformat+zeroPad(47,places), 
                      plateformat+zeroPad(56,places),
                      plateformat+zeroPad(64,places), 
                      plateformat+zeroPad(72,places), 
                      plateformat+zeroPad(80,places), 
                      plateformat+zeroPad(88,places), 
                      plateformat+zeroPad(97,places)),
                      
      createData('H', plateformat+zeroPad(8,places), 
                      plateformat+zeroPad(16,places), 
                      plateformat+zeroPad(24,places), 
                      plateformat+zeroPad(32,places) , 
                      plateformat+zeroPad(40,places) ,
                      plateformat+zeroPad(48,places), 
                      plateformat+zeroPad(57,places),
                      plateformat+zeroPad(65,places), 
                      plateformat+zeroPad(73,places), 
                      plateformat+zeroPad(81,places), 
                      'Intertek', 'Intertek'),
    ];



    return rows;
} 
const props = {
  rows: createDataBaseColumns('test'),  
}



afterEach(cleanup)
test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Plate96Wells {...props}></Plate96Wells>, div)
})


/*
test('Render correctly', () => {
  const { getByTestId } = render(<Plate96Wells {...props}></Plate96Wells>)
  expect(getByTestId('Plate96WellsTestId')).toBeInTheDocument()
})*/


 
