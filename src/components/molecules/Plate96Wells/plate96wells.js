import React, {useState }  from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import ValidateIcon from "@material-ui/icons/CheckCircleOutline";
import TableRow from '@material-ui/core/TableRow';
import { green, blue, grey } from '@material-ui/core/colors';

import {columns, createDataBaseColumns} from "utils/other/UtilsDesign"

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
});
var isfisrt= true;

const Plate96WellsMolecule = React.forwardRef((props, ref) => {
  const { label, children, ...rest } = props
  const classes = useStyles();
  const [rows, setRows] = useState(props.rows ?props.rows:[]);
    React.useEffect(()=>{setRows(createDataBaseColumns(props.plateName, 
      props.idPlate, props.idDesnsity, props.startIndex
      , props.quantitySample))
    },[])

    function setColorIcon (id, value){
      if (id=== 'col0')
      return (
        <div>
          {value}
        </div>
      )
      if (id!== 'col0' && value==='Vendor' )
      return (
        <div>
          <ValidateIcon fontSize='small' style={{ color: grey[900] }}/>
        </div>
      )
      if (id!== 'col0' && value!== '' && value!=='Vendor' )
      return (
        <div>
        <ValidateIcon fontSize='small' style={{ color: green[500] }}/>
        {value}
        </div>
      )


    }
    
  return (
    <Paper className={classes.root}
    
    data-testid={'Plate96WellsTestId'}
    >
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                  {columns.map((column) => {
                    const value = row[column.id];
                    return (
                      <TableCell key={column.id} align={column.align}>
                        {setColorIcon(column.id, value)}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>

    </Paper>
  )
})
// Type and required properties
Plate96WellsMolecule.propTypes = {
  label: PropTypes.String,
  children: PropTypes.node,
}
// Default properties
Plate96WellsMolecule.defaultProps = {
  label: 'Hello world',
  children: null,
}

export default Plate96WellsMolecule
