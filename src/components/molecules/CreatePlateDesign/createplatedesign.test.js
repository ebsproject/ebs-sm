import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import CreatePlateDesign from './createplatedesign'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
const props = {
  row:{
    original:{
      id:1,
      Total:94,
      Name:'Test',
    },
  },

}
afterEach(cleanup)
test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<CreatePlateDesign {...props}></CreatePlateDesign>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<CreatePlateDesign {...props}></CreatePlateDesign>)
  expect(getByTestId('CreatePlateDesignTestId')).toBeInTheDocument()
})
