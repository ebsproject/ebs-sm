import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';

import Divider from '@material-ui/core/Divider';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Grid from '@material-ui/core/Grid';
import TextField from 'components/atoms/TexField';
import FormControl from 'components/atoms/FormControl';
import TabPlate from 'components/molecules/TabPlate';
import { calculatePlates } from 'utils/other/UtilsDesign';
import { getbatchCodeById } from 'utils/apollo/SMclient';
import FormControlPlateDirecction from 'components/atoms/FormControlPlateDirection';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  dividerFullWidth: {
    margin: `5px 0 0 ${theme.spacing(2)}px`,
  },
  dividerInset: {
    margin: `5px 0 0 ${theme.spacing(9)}px`,
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

const CreatePlateDesignMolecule = React.forwardRef((props, ref) => {
  const { label, children, ...rest } = props;
  const classes = useStyles();
  const [open, setOpen] = React.useState(null);
  const [idPlate, setIdPlate] = React.useState(1);
  const [idDesnsity, setIdDensity] = React.useState(1);
  const [result, setResult] = React.useState(null);
  const [batchCode, setBatchCode] = React.useState(null);
  React.useEffect(() => {
    getbatchCodeById(1).then((response) => {
      setBatchCode(response);
    });
  }, []);

  const openTabPlates = () => {
    let result = calculatePlates(
      props.row.Total,
      96,
      props.row.Name,
      batchCode.lastPlateNumber,
      batchCode.lastSampleNumber,
      idDesnsity,
    );
    setOpen(
      <TabPlate
        dataTab={result}
        lastSample={batchCode.lastSampleNumber}
        idPlate={idPlate}
        idDesnsity={idDesnsity}
      ></TabPlate>,
    );
  };

  const closeWindow = () => {
    setOpen(null);
    props.handleClose();
    props.refresh();
  };

  const saveDesign = () => {
    props.actionSaveDesign(props.row.id, idPlate, idDesnsity, 1).then((reponse) => {
      props.handleClose();
      props.refresh();
    });
  };
  return (
    <div data-testid={'CreatePlateDesignTestId'}>
      <Dialog
        fullScreen
        open={props.open}
        onClose={closeWindow}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              edge='start'
              color='inherit'
              onClick={closeWindow}
              aria-label='close'
            >
              <CloseIcon />
            </IconButton>
            <Typography variant='h6' className={classes.title}>
              Create Plate
            </Typography>
            <Button autoFocus color='inherit' onClick={saveDesign}>
              Generate Plate
            </Button>
          </Toolbar>
        </AppBar>
        <Divider component='li' />
        <li>
          <Typography
            className={classes.dividerFullWidth}
            color='textSecondary'
            display='block'
            variant='caption'
          ></Typography>
        </li>
        <div className={classes.root}>
          <Grid container spacing={2}>
            <Grid container item xs={12} spacing={3}>
              <Grid item xs={4}>
                <TextField
                  label='Total Entities'
                  value={props.row.Total}
                  disable={'true'}
                />
              </Grid>
              <Grid item xs={4}>
                <TextField
                  label='Plate Name Prefix'
                  value={props.row.Name + '-P###'}
                  disable={'true'}
                />
              </Grid>
              <Grid item xs={4}>
                <TextField label='Tissue Type' value='Leaf' disable={'true'} />
              </Grid>
            </Grid>
            <Grid container item xs={12} spacing={3}>
              <Grid item xs={4}>
                <FormControl label='Select Control' handleChange={setIdDensity} />
              </Grid>
              <Grid item xs={4}>
                <FormControlPlateDirecction
                  label='Plate Fill  Direction'
                  setIdPlate={setIdPlate}
                />
              </Grid>
              <Grid item xs={4}>
                <Button variant='contained' color='primary' onClick={openTabPlates}>
                  Plate Preview
                </Button>
              </Grid>
            </Grid>
          </Grid>
          {open}
        </div>
      </Dialog>
    </div>
  );
});
// Type and required properties
CreatePlateDesignMolecule.propTypes = {
  label: PropTypes.String,
  children: PropTypes.node,
};
// Default properties
CreatePlateDesignMolecule.defaultProps = {
  label: 'Hello world',
  children: null,
};

export default CreatePlateDesignMolecule;
