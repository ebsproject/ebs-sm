import React from 'react'
import PropTypes from 'prop-types'
import {IconButton} from '@material-ui/core';
import SettingsIcon from '@material-ui/icons/Settings';
import CreatePlateDesign from "components/molecules/CreatePlateDesign"

const ConfigureBatchMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const { label, children, ...rest } = props

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
  
    setOpen(true);
  
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div
    data-testid={'ConfigureBatchTestId'}>
      
      {/* use this button to add a edit kind of action */}
        <IconButton
        //id = {`test-validate-${props.row.original.id}`}
        justIcon
        round
        simple
        onClick={handleClickOpen}
        color="primary"
        >
            <SettingsIcon />
        </IconButton>
        <CreatePlateDesign open={open} handleClose={handleClose} row={props.row} actionSaveDesign={props.actionSaveDesign}
        refresh = {props.refresh}
        />

    </div>
  )
})
// Type and required properties
ConfigureBatchMolecule.propTypes = {
  label: PropTypes.String,
  children: PropTypes.node,
}
// Default properties
ConfigureBatchMolecule.defaultProps = {
  label: 'Hello world',
  children: null,
}

export default ConfigureBatchMolecule
