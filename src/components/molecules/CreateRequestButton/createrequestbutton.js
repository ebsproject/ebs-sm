import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS AND ATOMS TO USE
import EbsForm from "ebs-form";
import { makeStyles } from "@material-ui/core/styles";
import AddIcon from "@material-ui/icons/Add";
import { useDispatch } from "react-redux";
import {
  Grid,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  AppBar,
  Tabs,
  Tab,
  Box,
} from "@material-ui/core";
import { FormattedMessage } from "react-intl";
import { SMgraph } from "utils/apollo";
import { CREATE_REQUEST } from "utils/apollo/gql/RequestManager";
import GeneralForm from "components/atoms/GeneralForm";
import ServiceProviderForm from "components/atoms/ServiceProviderForm";
import EntryListForm from "components/atoms/EntryListform";
import { formatDate } from "utils/other/generalFunctions";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  paper: {
    minHeight: "80vh",
    maxHeight: "80vh",
  },
  button: {
    margin: theme.spacing(1),
  },
}));

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const CreateRequestButtonMolecule = React.forwardRef((props, ref) => {
  const { refresh, ...rest } = props;
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [open, setOpen] = React.useState(false);
  // State to save form data
  const [general, setGeneral] = React.useState(null);
  const [serviceProvider, setServiceProvider] = React.useState(null);

  const dispatch = useDispatch();

  function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && <Box p={3}>{children}</Box>}
      </div>
    );
  }

  function transformDate(inputDate) {
    return formatDate(inputDate);
  }
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };

  const handleClickOpen = () => {
    setValue(0);
    setGeneral(null)
    setServiceProvider(null)
    setOpen(true);
  };

  const handleClose = (value) => {
    setOpen(false);
  };

  const handleChange = (newValue) => {
    setValue(newValue);
  };

  async function onSubmit(formData) {
    const { data, error, loading } = await SMgraph.mutate({
      mutation: CREATE_REQUEST,
      variables: {
        RequestTo: {
          id: 0,
          requester: general.requester,
          requestCode: general.requestCodeHidden,
          submitionDate: transformDate(general.submitionDate),
          adminContactName: general.adminContactName,
          completedBy: transformDate(general.completedBy),
          icc: general.icc,
          description: general.description,
          requesterEmail: general.requesterEmail,
          adminContactEmail: general.adminContactEmail,
          listId: Number(formData.listId.value),
          totalEntities: formData.hiddenList,
          tenant: 1,
          crop: { id: Number(general.crop.value) },
          program: { id: Number(serviceProvider.program.value) },
          purpose: { id: Number(serviceProvider.purpose.value) },
          serviceprovider: {
            id: Number(serviceProvider.serviceprovider.value),
          },
          status: { id: 1 },
          servicetype: { id: Number(serviceProvider.servicetype.value) },
          service: { id: Number(serviceProvider.service.value) },
        },
      },
    });

    if (data.createRequest) {
      dispatch({
        type: "ALERT_MESSAGE",
        payload: {
          message: "Request created",
          translationId: "request.created",
          severity: "success",
        },
      });
      handleClose();
      refresh();
    }
  }

  async function onSubmitGeneral(formData) {
    setGeneral(formData);
    handleChange(value + 1);
  }

  async function onSubmitServiceProvider(formData) {
    setServiceProvider(formData);
    handleChange(value + 1);
  }

  return (
    /* 
     @prop data-testid: Id to use inside createrequestbutton.test.js file.
     */
    <div ref={ref} data-testid={"CreateRequestButtonTestId"}>
      <Button
        variant="contained"
        color="primary"
        className={classes.button}
        startIcon={<AddIcon />}
        onClick={handleClickOpen}
      >
        Create
      </Button>
      <Dialog
        // className={classes.paper}
        fullWidth
        maxWidth="md"
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
          >
            <Grid item>{"New Request"}</Grid>
          </Grid>
        </DialogTitle>
        <DialogContent>
          <div className={classes.root}>
            <AppBar position="static">
              <Tabs value={value} aria-label="New Request Tabs">
                <Tab label="General" {...a11yProps(0)} />
                <Tab label="Service Provider" {...a11yProps(1)} />
                <Tab label="Entry List" {...a11yProps(2)} />
              </Tabs>
            </AppBar>
            <TabPanel value={value} index={0}>
              <div style={{ minHeight: "330px" }}>
                <GeneralForm
                  general={general}
                  onSubmitGeneral={onSubmitGeneral}
                />
              </div>
            </TabPanel>
            <TabPanel value={value} index={1}>
              <div style={{ minHeight: "330px" }}>
                <ServiceProviderForm
                  value={value}
                  handleChange={handleChange}
                  serviceProvider={serviceProvider}
                  onSubmitServiceProvider={onSubmitServiceProvider}
                />
              </div>
            </TabPanel>
            <TabPanel value={value} index={2}>
              <div style={{ minHeight: "330px" }}>
                <EntryListForm
                  value={value}
                  handleChange={handleChange}
                  onSubmit={onSubmit}
                />
              </div>
            </TabPanel>
          </div>
        </DialogContent>
      </Dialog>
    </div>
  );
});
// Type and required properties
CreateRequestButtonMolecule.propTypes = {};
// Default properties
CreateRequestButtonMolecule.defaultProps = {};

export default CreateRequestButtonMolecule;
