import React, {useState, useEffect } from 'react'
import PropTypes from 'prop-types'

import ReactTable from "react-table";
import "react-table/react-table.css";
import "assets/scss/material-dashboard-pro-react/plugins/_plugin-react-table.css";
import {getBatchByID} from 'utils/apolloClientSampleManager';
import Pagination from "components/molecules/Pagination/Pagination"
import Detail from "components/molecules/Details/Detail"

const getDetails = (rowData) => {
  const dataActions = rowData.map(item => {
  let actions;
          actions = (
              <table align={'center'}>
                  <tbody>
                      <tr>
                          <td>
                          <Detail
                          page={0}
                          batchId={item.id}
                          />            
                          </td>
                      </tr>
                      
                  </tbody>
                  
              </table>
          )
          return {
              ...item,
              actions
          }
      }
  )
  return dataActions;
}


const DetailBatchMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const { label, children, ...rest } = props
  const [detailData, setDetailData] = useState([]);
  React.useEffect(()=>{getBatchByID(props.idBatch).then(response => {
    setDetailData(getDetails(response))
    }
  )   
  },[])
  return (
        <div
        data-testid={'DetailBatchTestId'}>
                            <ReactTable
                            
                                noDataText=""
                                filterable
                                data={detailData}
                                defaultPageSize={5}
                                PaginationComponent={Pagination}
                                className="-striped -highlight"
                                columns={[
                                  {
                                    Header: "Actions",
                                    width: 80, 
                                    accessor: "actions"
                                    
                                  },
                                  {
                                    Header: "Request",
                                    accessor : "Request_Code"
                                    
                                  },
                                  {
                                    Header: "Crop",
                                    accessor: "Crop"
                                    
                                  },
                                  {
                                    Header: "Program",
                                    accessor: "Program"
                                    
                                  },
                                  {
                                    Header: "ICC",
                                    accessor: "ICC_Code"
                                    
                                  }
                                  ,
                                  {
                                    Header: "Purpose",
                                    accessor: "Purpose"
                                    
                                  } ,
                                  {
                                    Header: "Service",
                                    accessor: "Service"
                                    
                                  }
                                  ,
                                  {
                                    Header: "Total",
                                    accessor: "Total",
                                    align: "center"
                                    
                                  },
                                  {
                                    Header: "id",
                                    accessor: "id",
                                    show: false  
                                  },
                                  
                                ]}
                            /> 
      </div>   
  )
})
// Type and required properties
DetailBatchMolecule.propTypes = {
  label: PropTypes.String,
  children: PropTypes.node,
}
// Default properties
DetailBatchMolecule.defaultProps = {
  children: null,
}

export default DetailBatchMolecule
