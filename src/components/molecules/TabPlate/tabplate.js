import React from 'react'
import PropTypes from 'prop-types';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import SwipeableViews from 'react-swipeable-views';
import Plate96Wells from "components/molecules/Plate96Wells"
function TabPanel(props) {
  const { children, value, index, plateName , idPlate, idDesnsity
    ,startIndex, quantitySample } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}

    >
      {value === index && (
          
        <Plate96Wells plateName= {plateName} 
        idPlate={idPlate} idDesnsity={idDesnsity}
        startIndex= {startIndex}
        quantitySample={quantitySample}
        ></Plate96Wells>
        
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));
const TabPlateMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const { label, children, ...rest } = props
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const theme = useTheme();
  const handleChange = (event, newValue) => {
    setValue(newValue);
  }
  const handleChangeIndex = (index) => {
    setValue(index);
  };
  return (
    <div className={classes.root}
    data-testid={'TabPlateTestId'}>
      <AppBar position="static" color="primary">
        <Tabs value={value} 
        onChange={handleChange} 
        aria-label="simple tabs example"
        indicatorColor="primary"
        variant="fullWidth"
        >
          {props.dataTab.map((element) => (
              <Tab label={element.plate} {...a11yProps(element.id)} />
              )
            )
          }
        </Tabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
        >
      {props.dataTab.map((element) => (
         <TabPanel value={value} index={element.id} dir={theme.direction} 
         plateName={element.plate} idPlate={props.idPlate} 
         idDesnsity={props.idDesnsity} 
         startIndex = {element.startIndex}
         quantitySample = {element.sampleQuantity}
         >
            {element.plate}
           </TabPanel>
        )
      )
    }  
     </SwipeableViews>
    </div>
  )
})
// Type and required properties
TabPlateMolecule.propTypes = {
  label: PropTypes.String,
  children: PropTypes.node,
}
// Default properties
TabPlateMolecule.defaultProps = {
  label: 'Hello world',
  children: null,
}

export default TabPlateMolecule
