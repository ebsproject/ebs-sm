import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import StatusActionDesign from './statusactiondesign'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
const props = {
  row:{
    original:{
      id:1,
      actionNext:'actionNext',
      actionReject:'actionReject',
    },
  },

}
afterEach(cleanup)
test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<StatusActionDesign {...props}></StatusActionDesign>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<StatusActionDesign {...props}></StatusActionDesign>)
  expect(getByTestId('StatusActionDesignTestId')).toBeInTheDocument()
})
