import React, { useState } from 'react';
import PropTypes from 'prop-types';
import EbsGrid from 'ebs-grid-lib';
import { Typography } from '@material-ui/core';
import { B4Rrest } from 'utils/axios';
import { FormattedMessage } from 'react-intl';
// CORE COMPONENTS AND ATOMS TO USE
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
} from '@material-ui/core';
import { Visibility } from '@material-ui/icons';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/

const columns = [
  {
    Header: 'Id',
    accessor: 'plotDbId',
    hidden: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Entry Number'} />,
    accessor: 'entryNumber',
    disableFilters: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Entry Type'} />,
    accessor: 'entryType',
    filters: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Plot Code'} />,
    accessor: 'plotCode',
    filters: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Plot QC Code'} />,
    accessor: 'plotQcCode',
    filters: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Plot Status'} />,
    accessor: 'plotStatus',
    filters: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Plot Number'} />,
    accessor: 'plotNumber',
    filters: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Plot Type'} />,
    accessor: 'plotType',
    filters: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Rep'} />,
    accessor: 'rep',
    filters: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Design X'} />,
    accessor: 'designX',
    filters: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Design Y'} />,
    accessor: 'designY',
    filters: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Field X'} />,
    accessor: 'fieldX',
    filters: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Field Y'} />,
    accessor: 'fieldY',
    filters: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Pa X'} />,
    accessor: 'paX',
    filters: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'PaY'} />,
    accessor: 'paY',
    filters: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Block Number'} />,
    accessor: 'blockNumber',
    filters: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Harvest Status'} />,
    accessor: 'harvestStatus',
    filters: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Entry Name'} />,
    accessor: 'entryName',
    filters: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Designation'} />,
    accessor: 'designation',
    filters: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Parentage'} />,
    accessor: 'parentage',
    filters: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'State'} />,
    accessor: 'state',
    filters: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Crop Code'} />,
    accessor: 'cropCode',
    filters: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Creator'} />,
    accessor: 'creator',
    filters: true,
  },
];

const ViewEntryListButtonMolecule = React.forwardRef((props, ref) => {
  const { rowData, ...rest } = props;
  const [data, setData] = useState(null);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(null);
  const [totalElements, setTotalElements] = useState(null);
  const [open, setOpen] = useState(false);

  async function fetch(page, pageSize, columnsToFilter, value) {
    try {
      const response = await B4Rrest.get(`/lists/${rowData.listDbId}/members`);
      const data = response.data.result.data[0].members;

      // Dummy data
      const plotData = [
        {
          plotDbId: 1054,
          locationDbId: 170,
          occurrenceDbId: 170,
          entryDbId: 27852,
          entryNumber: 1,
          entryType: 'entry',
          plotCode: '1',
          plotQcCode: 'G',
          plotStatus: 'active',
          plotNumber: 1,
          plotType: 'plot',
          rep: 1,
          designX: 1,
          designY: 1,
          fieldX: 1,
          fieldY: 1,
          paX: null,
          paY: null,
          blockNumber: 1,
          harvestStatus: 'NO_HARVEST',
          experimentDbId: 9,
          entryName: 'IR 107968',
          germplasmDbId: 48113,
          designation: 'IR 107968',
          parentage: 'IR04A115/IR06N155',
          state: 'not_fixed',
          cropDbId: 1,
          cropCode: 'RICE',
          creatorDbId: 1,
          creator: 'Irri, Bims',
          modifierDbId: null,
          modifier: null,
        },
        {
          plotDbId: 1055,
          locationDbId: 170,
          occurrenceDbId: 170,
          entryDbId: 27853,
          entryNumber: 2,
          entryType: 'entry',
          plotCode: '2',
          plotQcCode: 'G',
          plotStatus: 'active',
          plotNumber: 2,
          plotType: 'plot',
          rep: 1,
          designX: 2,
          designY: 1,
          fieldX: 2,
          fieldY: 1,
          paX: null,
          paY: null,
          blockNumber: 1,
          harvestStatus: 'NO_HARVEST',
          experimentDbId: 9,
          entryName: 'IR 107975',
          germplasmDbId: 48114,
          designation: 'IR 107975',
          parentage: 'IR07A179/IR09A228',
          state: 'not_fixed',
          cropDbId: 1,
          cropCode: 'RICE',
          creatorDbId: 1,
          creator: 'Irri, Bims',
          modifierDbId: null,
          modifier: null,
        },
        {
          plotDbId: 1056,
          locationDbId: 170,
          occurrenceDbId: 170,
          entryDbId: 27854,
          entryNumber: 3,
          entryType: 'entry',
          plotCode: '3',
          plotQcCode: 'G',
          plotStatus: 'active',
          plotNumber: 3,
          plotType: 'plot',
          rep: 1,
          designX: 3,
          designY: 1,
          fieldX: 3,
          fieldY: 1,
          paX: null,
          paY: null,
          blockNumber: 1,
          harvestStatus: 'NO_HARVEST',
          experimentDbId: 9,
          entryName: 'IR 107976',
          germplasmDbId: 48115,
          designation: 'IR 107976',
          parentage: 'IR09A228/IR06N155',
          state: 'not_fixed',
          cropDbId: 1,
          cropCode: 'RICE',
          creatorDbId: 1,
          creator: 'Irri, Bims',
          modifierDbId: null,
          modifier: null,
        },
      ];

      /**
       * forEach using async/await
       *
       * This code block will be used once the plot mock data has been created.
       * At the moment, the data that will be displayed by the table is
       * hardcoded.
       */
      // const plots = await Promise.all(
      //   data.map(async (listMember) => {
      //     const body = {
      //       fields:
      //         'plot.id AS plotDbId | plot.location_id AS locationDbId | plot.occurrence_id AS occurrenceDbId | entry.id AS entryDbId | entry.entry_number AS entryNumber | entry.entry_type AS entryType | plot.plot_code AS plotCode | plot.plot_qc_code AS plotQcCode | plot.plot_status AS plotStatus | plot.plot_number AS plotNumber | plot.plot_type AS plotType | plot.rep AS rep | plot.design_x AS designX | plot.design_y AS designY | plot.field_x AS fieldX | plot.field_y AS fieldY | plot.pa_x AS paX | plot.pa_y AS paY | plot.block_number AS blockNumber | plot.harvest_status AS harvestStatus | occurrence.experiment_id AS experimentDbId | entry.entry_name AS entryName | entry.germplasm_id AS germplasmDbId | germplasm.designation AS designation | germplasm.parentage AS parentage | germplasm.germplasm_state AS state | crop.id AS cropDbId | crop.crop_code AS cropCode | creator.id AS creatorDbId | creator.person_name AS creator | modifier.id AS modifierDbId | modifier.person_name AS modifier',
      //       plotDbId: listMember.listMemberDbId,
      //     }
      //     const plotResponse = await B4Rrest.post('/plots-search', body)
      //     return plotResponse
      //   })
      // )

      // Note the number of entries

      setData(plotData);
      setPage(page);
      setTotalPages(1);
      // setTotalPages(response.data.metadata.pagination.totalPages)
    } catch (error) {
      throw new Error(error);
    }
  }

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    /*
     @prop data-testid: Id to use inside viewentrylistbutton.test.js file.
     */
    <div ref={ref} data-testid={'ViewEntryListButtonTestId'}>
      <IconButton title='View' color='primary' onClick={handleOpen}>
        <Visibility />
      </IconButton>
      <Dialog fullWidth={true} maxWidth='lg' open={open} onClose={handleClose}>
        <DialogTitle>
          <FormattedMessage
            id={'some.id'}
            defaultMessage={`View Plot List of: ${rowData.name}`}
          />
        </DialogTitle>
        <DialogContent>
          {data?.length === 0 && (
            <Typography>
              <FormattedMessage
                id={'some.id'}
                defaultMessage={'No items to display.'}
              />
            </Typography>
          )}

          {data?.length !== 0 && (
            <EbsGrid
              toolbar={true}
              columns={columns}
              fetch={fetch}
              data={data}
              page={page}
              select='single'
              totalPages={totalPages}
              totalElements={totalElements}
              pagination
              indexing
              callstandard='brapi'
            />
          )}
        </DialogContent>
        <DialogActions>
          <Button
            variant='contained'
            color='primary'
            autoFocus
            onClick={handleClose}
          >
            <FormattedMessage id={'some.id'} defaultMessage={'OK'} />
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
});
// Type and required properties
ViewEntryListButtonMolecule.propTypes = {
  rowData: PropTypes.object,
};
// Default properties
ViewEntryListButtonMolecule.defaultProps = {};

export default ViewEntryListButtonMolecule;
