import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS AND ATOMS TO USE
import { makeStyles } from "@material-ui/core";
import AcceptButton from "components/atoms/AcceptButton";
import RejectButton from "components/atoms/RejectButton";
import ValidateButton from "components/atoms/ValidateButton";

const useStyles = makeStyles((theme) => ({
  rowActionStyle: {
    display: "flex",
    alignItems: "stretch",
  },
}));
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const RequestStatusGridRowActionsMolecule = React.forwardRef((props, ref) => {
  const classes = useStyles();

  return (
    /* 
     @prop data-testid: Id to use inside requeststatusgridrowactions.test.js file.
     */
    <div
      className={classes.rowActionStyle}
      data-testid={"RequestStatusGridRowActionsTestId"}
    >
      <AcceptButton {...props} />
      <RejectButton {...props} />
      <ValidateButton {...props} />
    </div>
  );
});
// Type and required properties
RequestStatusGridRowActionsMolecule.propTypes = {
  rowData: PropTypes.object,
  refresh: PropTypes.func,
};
// Default properties
RequestStatusGridRowActionsMolecule.defaultProps = {};

export default RequestStatusGridRowActionsMolecule;
