import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS AND ATOMS TO USE
import { useDispatch } from "react-redux";
import {Typography, IconButton} from '@material-ui/core'
import { Button } from "components/atoms/CustomButtons";
import RejectIcon from "@material-ui/icons/Cancel";
import { FormattedMessage } from "react-intl";
import ConfirmDialogRaw from "components/atoms/ConfirmDialogRaw";
import { SMgraph } from "utils/apollo";
import { MODIFY_REQUEST } from "utils/apollo/gql/RequestManager";

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const RejectButtonMolecule = React.forwardRef((props, ref) => {
  const { rowData, refresh, children, ...rest } = props;
  const [open,setOpen] = React.useState(false);
  const dispatch = useDispatch();

  const rejectedRequest = async (query) => {
    const STATUS_ID_REJECTED = 3;
    const response = await SMgraph.mutate({
      mutation: MODIFY_REQUEST,
      variables: {
        RequestTo: {
          id: rowData.id,
          status: { id: STATUS_ID_REJECTED },
        },
      },
    });
    if (response.data) successReject();
  };

  const warningMessage = () => {
    setOpen(true);
  };

  const successReject = () => {
    //do other stuff
    setOpen(false);
    refresh();
    dispatch({
      type: "ALERT_MESSAGE",
      payload: {
        message: "Request rejected",
        translationId: "request.rejected",
        severity: "warning",
      },
    });
  };

  const cancelReject = () => {
    //do other stuff
    setOpen(false)
  };

  return (
    /* 
     @prop data-testid: Id to use inside rejectbutton.test.js file.
     */
    <div data-testid={"RejectButtonTestId"}>
      <ConfirmDialogRaw
        open={open}
        title={
          <FormattedMessage
            id={"sm.rejectreq.warning.title"}
            defaultMessage={"Are you sure?"}
          />
        }
        onOk={rejectedRequest}
        onCancel={cancelReject}
      >
        <Typography variant="body1">
          <FormattedMessage
            id={"sm.rejectreq.warning.message"}
            defaultMessage={"Do you want reject this request?"}
          />
        </Typography>
      </ConfirmDialogRaw>
      <IconButton
        // id={`test-reject-${props.row.original.id}`}
        justIcon
        round
        simple
        onClick={warningMessage}
        color="primary"
        className="remove"
      >
        <RejectIcon />
      </IconButton>
    </div>
  );
});
// Type and required properties
RejectButtonMolecule.propTypes = {};
// Default properties
RejectButtonMolecule.defaultProps = {};

export default RejectButtonMolecule;
