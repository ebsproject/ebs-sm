import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import RejectButton from './rejectbutton'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<RejectButton></RejectButton>, div)
})

// Props to send component to be rendered
const props = {}

test('Render correctly', () => {
  const { getByTestId } = render(<RejectButton {...props}></RejectButton>)
  expect(getByTestId('RejectButtonTestId')).toBeInTheDocument()
})
