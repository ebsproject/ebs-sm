import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS AND ATOMS TO USE
import { IconButton, makeStyles } from "@material-ui/core";
import ErrorOutlineOutlined from "@material-ui/icons/ErrorOutlineOutlined";

const useStyles = makeStyles((theme) => ({
  ErrorOutlineOutlinedStyle: {
    fill: "#00bcd4",
  },
}));

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ValidateButtonMolecule = React.forwardRef((props, ref) => {
  const classes = useStyles();
  const { rowData, refresh, ...rest } = props;

  return (
    /* 
     @prop data-testid: Id to use inside validatebutton.test.js file.
     */
    <IconButton
      ref={ref}
      data-testid={"ValidateButtonTestId"}
      title="Validate"
      onClick={() => {
        alert(JSON.stringify(rowData));
        refresh();
      }}
    >
      <ErrorOutlineOutlined className={classes.ErrorOutlineOutlinedStyle} />
    </IconButton>
  );
});
// Type and required properties
ValidateButtonMolecule.propTypes = {};
// Default properties
ValidateButtonMolecule.defaultProps = {};

export default ValidateButtonMolecule;
