import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepButton from '@material-ui/core/StepButton';
import StepConnector from '@material-ui/core/StepConnector';
import AssignmentIcon from '@material-ui/icons/Assignment';
import GroupWorkTwoToneIcon from '@material-ui/icons/GroupWorkTwoTone';
import CardTravelIcon from '@material-ui/icons/CardTravel';
import BorderColorOutlinedIcon from '@material-ui/icons/BorderColorOutlined';


import { Link } from "react-router-dom";
import { FormattedMessage } from 'react-intl'

const ColorlibConnector = withStyles({
  alternativeLabel: {
    top: 22,
  },
  active: {
    '& $line': {
      backgroundImage:
        'linear-gradient( 95deg,rgb(2,113,33) 0%,rgb(2,64,87) 50%,rgb(2,35,135) 100%)',
    },
  },
  completed: {
    '& $line': {
      backgroundImage:
        'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)',
    },
  },
  line: {
    height: 3,
    border: 0,
    backgroundColor: '#eaeaf0',
    borderRadius: 1,
  },
})(StepConnector);


const useColorlibStepIconStyles = makeStyles({
  root: {
    backgroundColor: '#ccc',
    zIndex: 1,
    color: '#fff',
    width: 50,
    height: 50,
    display: 'flex',
    borderRadius: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  active: {
    backgroundImage:
      'linear-gradient( 136deg, rgb(2,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)',
    boxShadow: '0 4px 10px 0 rgba(0,0,0,.25)',
  },
  completed: {
    backgroundImage:
      'linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)',
  },
});


function ColorlibStepIcon(props) {
  const classes = useColorlibStepIconStyles();
  const { active, completed } = props;

  const icons = {
    1: <AssignmentIcon />,
    2: <GroupWorkTwoToneIcon />,
    3: <BorderColorOutlinedIcon />,
    4:<CardTravelIcon/>,
  };

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
        [classes.completed]: completed,
      })}
    >
      {icons[String(props.icon)]}
    </div>
  );
}

ColorlibStepIcon.propTypes = {
  /**
   * Whether this step is active.
   */
  active: PropTypes.bool,
  /**
   * Mark the step as completed. Is passed to child components.
   */
  completed: PropTypes.bool,
  /**
   * The label displayed in the step icon.
   */
  icon: PropTypes.node,
};





export default function CustomizedSteppers() {
  const [activeStep, setActiveStep] = React.useState(0);

  return (
    <>
      <Stepper alternativeLabel activeStep={activeStep} connector={<ColorlibConnector />}>
        <Step>
          <StepButton onClick={() => { setActiveStep(0) }} component={Link} to={"/request"} disabled={false} completed={false}>
            <StepLabel StepIconComponent={ColorlibStepIcon}>
              <FormattedMessage id={'sm.step.lbl.req'} defaultMessage={"Request"} />
            </StepLabel>
          </StepButton>
        </Step>
        <Step>
          <StepButton onClick={() => { setActiveStep(1) }} component={Link} to={"/inspect"} disabled={false} completed={false}>
            <StepLabel StepIconComponent={ColorlibStepIcon}>
              <FormattedMessage id={'sm.step.lbl.bat'} defaultMessage={"Batch"} />
            </StepLabel>
          </StepButton>
        </Step>
        <Step>
          <StepButton onClick={() => { setActiveStep(2) }} component={Link} to={"/design"} disabled={false} completed={false}>
            <StepLabel StepIconComponent={ColorlibStepIcon}>
            <FormattedMessage id={'sm.step.lbl.bat'} defaultMessage={"Design"} />
            </StepLabel>
          </StepButton>
        </Step>

      </Stepper>
    </>
  );
}
