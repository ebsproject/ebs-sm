import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
// CORE COMPONENTS AND ATOMS TO USE
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  DialogContent,
  Typography,
  makeStyles,
  Tabs,
  Tab,
  Paper,
  Card,
  CardHeader,
  CardContent,
  Container,
  Table,
  TableHead,
  TableCell,
  TableBody,
  TableRow,
  IconButton,
} from "@material-ui/core";

import { FormattedMessage } from "react-intl";

import { Info } from "@material-ui/icons";
import { Visibility } from "@material-ui/icons";
import { SMgraph } from "utils/apollo";
import { FIND_ENTRY_LIST_LIST } from "utils/apollo/gql/RequestManager";

const useStyles = makeStyles((theme) => ({
  wrapIcon: {
    verticalAlign: "middle",
    display: "inline-flex",
  },
  grayBackground: {
    background: "#FAFAFA",
  },
  bold: {
    fontWeight: 600,
  },
  marginRight: {
    marginRight: 20,
  },
  statusBadge: {
    background: "#2096F3",
    borderRadius: 3,
    color: "white",
    fontWeight: 700,
    padding: 5,
    textTransform: "uppercase",
  },
  table: {
    maxWidth: 500,
  },
  tableCell: {
    borderBottom: "none",
    paddingRight: 0,
  },
}));

const notSetLabel = (
  <Typography component="span" color="primary">
    <Box fontStyle="italic">(not set)</Box>
  </Typography>
);

const rearrangeRowData = (rowData) => {
  /**
   * This is used for rearranging the keys of the rowData
   * and prioritize the "Status" key
   */
  if (!rowData) return;
  const reArrangedRowData = {
    status: {
      label: "Status",
      value: rowData.status,
    },
    requestCode: {
      label: "Request Code",
      value: rowData.requestCode,
    },
    requester: {
      label: "Requester",
      value: rowData.requester,
    },
    service: {
      label: "Service",
      value: rowData.service,
    },
    serviceprovider: {
      label: "Service Provider",
      value: rowData.serviceprovider,
    },
    servicetype: {
      label: "Service Type",
      value: rowData.servicetype,
    },
    purpose: {
      label: "Purpose",
      value: rowData.purpose,
    },
    crop: {
      label: "Crop",
      value: rowData.crop,
    },
    totalEntities: {
      label: "Total Entities",
      value: rowData.totalEntities,
    },
  };

  return reArrangedRowData;
};

const ENTRY_LIST_TABLE_HEADERS = [
  {
    formattedMessageId: "some.id",
    defaultMessage: "LIST NAME",
  },
  {
    formattedMessageId: "some.id",
    defaultMessage: "EXPERIMENT NAME",
  },
  {
    formattedMessageId: "some.id",
    defaultMessage: "OCCURENCE CODE",
  },
  {
    formattedMessageId: "some.id",
    defaultMessage: "SITE NAME",
  },
  {
    formattedMessageId: "some.id",
    defaultMessage: "FIELD NAME",
  },
  {
    formattedMessageId: "some.id",
    defaultMessage: "EXPERIMENT YEAR",
  },
  {
    formattedMessageId: "some.id",
    defaultMessage: "EXPERIMENT SEASON",
  },
  {
    formattedMessageId: "some.id",
    defaultMessage: "EXPERIMENT ENTRY NUMBER",
  },
  {
    formattedMessageId: "some.id",
    defaultMessage: "PLOT CODE",
  },
  {
    formattedMessageId: "some.id",
    defaultMessage: "GERMPLASM CODE",
  },
  {
    formattedMessageId: "some.id",
    defaultMessage: "PEDIGREE",
  },
  {
    formattedMessageId: "some.id",
    defaultMessage: "SEED GENERATION",
  },
  {
    formattedMessageId: "some.id",
    defaultMessage: "SEED CODE",
  },
  {
    formattedMessageId: "some.id",
    defaultMessage: "MATERIAL TYPE",
  },
  {
    formattedMessageId: "some.id",
    defaultMessage: "X",
  },
  {
    formattedMessageId: "some.id",
    defaultMessage: "Y",
  },
  {
    formattedMessageId: "some.id",
    defaultMessage: "MATERIAL CLASS",
  },
  {
    formattedMessageId: "some.id",
    defaultMessage: "PLANT NUMBER",
  },
];

function createData(data) {
  return {
    id: data.id,
    listName: data.listName,
    experiementName: data.experiementName,
    occurrenceCode: data.occurrenceCode,
    siteName: data.siteName,
    fieldName: data.fieldName,
    experimentYear: data.experimentYear,
    experimentSeason: data.experimentSeason,
    expEntryNumber: data.expEntryNumber,
    plotCode: data.plotCode,
    germplasmCode: data.germplasmCode,
    pedigree: data.pedigree,
    seedGeneration: data.seedGeneration,
    seedCode: data.seedCode,
    materialType: data.materialType,
    x: data.x,
    y: data.y,
    materialClass: data.materialClass,
    plantNumber: data.plantNumber,
  };
}

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ViewRequestButtonMolecule = React.forwardRef((props, ref) => {
  const classes = useStyles();
  const { rowData, refresh, ...rest } = props;
  const [open, setOpen] = React.useState(false);
  const [entryListData, setEntryListData] = useState(null);

  const entryListTableRows = entryListData?.map((data) => createData(data));

  const reArrangedRowData = rearrangeRowData(rowData);
  const [value, setValue] = useState(0);

  const handleOpen = () => {
    getEntryListData();
    setOpen(true);
  };

  function getEntryListData() {
    SMgraph
      .query({ query: FIND_ENTRY_LIST_LIST })
      .then(({ data }) => {
        setEntryListData(data.findPlantEntryListList.content);
      })
      .catch((error) => {
        throw new Error(error);
      });
  }

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
    };
  }

  function TabPanel(props) {
    const { children, value, index } = props;

    return (
      <div role="tabpanel" hidden={value !== index}>
        <br />
        {value === index && (
          <Card>
            {index === 0 && (
              <CardHeader
                title={
                  <Typography variant="body2" className={classes.bold}>
                    <FormattedMessage
                      id={"some.id"}
                      defaultMessage={"Basic Information"}
                    />
                  </Typography>
                }
              />
            )}
            <CardContent className={classes.grayBackground}>
              {children}
            </CardContent>
          </Card>
        )}
      </div>
    );
  }

  function tableValue(value) {
    /**
     * This function is to check if the value of the row from the rowData
     * is a string or null. This is because a request can be created
     * in which some of its values can be an empty string or a
     * null value.
     */
    if (value === "" || value === null) return notSetLabel;
    return value;
  }

  return (
    /* All ViewButton component displayed in every row (including Dialog component) */
    /*
     @prop data-testid: Id to use inside viewrequestbutton.test.js file.
     */
    <div ref={ref} data-testid={"ViewRequestButtonTestId"}>
      <IconButton
        title="View"
        color="primary"
        onClick={() => {
          handleOpen();
        }}
      >
        <Visibility />
      </IconButton>
      <Dialog fullWidth={true} maxWidth="lg" open={open} onClose={handleClose}>
        <DialogTitle id="responsive-dialog-title">
          <Typography component="span" className={classes.wrapIcon}>
            <Info /> {reArrangedRowData["requestCode"].value}
          </Typography>
        </DialogTitle>
        <DialogContent className={classes.grayBackground}>
          <Paper square>
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label="simple tabs example"
            >
              <Tab
                label={
                  <FormattedMessage id={"some.id"} defaultMessage={"BASIC"} />
                }
                {...a11yProps(0)}
              />
              <Tab
                label={
                  <FormattedMessage id={"some.id"} defaultMessage={"ENTRY"} />
                }
                {...a11yProps(1)}
              />
            </Tabs>
          </Paper>
          <TabPanel value={value} index={0}>
            <Container maxWidth="md">
              <Table className={classes.table} size="small">
                <TableBody>
                  {reArrangedRowData &&
                    Object.keys(reArrangedRowData).map((key, index) => (
                      <TableRow key={key}>
                        <TableCell
                          className={`${classes.tableCell} ${classes.bold}`}
                          align="right"
                        >
                          <FormattedMessage
                            id={"some.id"}
                            defaultMessage={`${reArrangedRowData[key].label}`}
                          />
                        </TableCell>
                        <TableCell className={classes.tableCell} align="left">
                          <span
                            className={clsx({
                              [classes.statusBadge]: key === "status",
                            })}
                          >
                            {tableValue(reArrangedRowData[key].value)}
                          </span>
                        </TableCell>
                      </TableRow>
                    ))}
                </TableBody>
              </Table>
            </Container>
          </TabPanel>
          <TabPanel value={value} index={1}>
            <Table stickyHeader size="small">
              <TableHead>
                <TableRow>
                  {ENTRY_LIST_TABLE_HEADERS.map((row) => (
                    <TableCell align="center" key={row.defaultMessage}>
                      <FormattedMessage
                        id={"some.id"}
                        defaultMessage={row.defaultMessage}
                      />
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {entryListTableRows?.map((row) => (
                  <TableRow key={row.id}>
                    {Object.keys(row).map((key) => {
                      if (key !== "id") {
                        return (
                          <TableCell key={key} align="center">
                            {row[key]}
                          </TableCell>
                        );
                      }
                    })}
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TabPanel>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            color="primary"
            autoFocus
            onClick={handleClose}
          >
            <FormattedMessage id={"some.id"} defaultMessage={"OK"} />
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
});
// Type and required properties
ViewRequestButtonMolecule.propTypes = {};
// Default properties
ViewRequestButtonMolecule.defaultProps = {};

export default ViewRequestButtonMolecule;
