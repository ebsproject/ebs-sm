import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS AND ATOMS TO USE
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  DialogContent,
  makeStyles,
  Typography,
  IconButton,
} from "@material-ui/core";

import { Delete, Send, Warning } from "@material-ui/icons";
import { DeleteForever } from "@material-ui/icons";
import { FormattedMessage } from "react-intl";
import { useDispatch } from "react-redux";
import { DELETE_REQUEST } from "utils/apollo/gql/RequestManager";
import { SMgraph } from "utils/apollo";

const useStyles = makeStyles(() => ({
  dialog: {
    position: "absolute",
    left: "27%",
    top: 35,
  },
  grayBackground: {
    background: "#FAFAFA",
  },
  wrapIcon: {
    verticalAlign: "middle",
    display: "inline-flex",
  },
}));

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const DeleteRequestButtonMolecule = React.forwardRef((props, ref) => {
  const classes = useStyles();
  const { rowData, refresh, ...rest } = props;
  const [open, setOpen] = React.useState(false);
  const dispatch = useDispatch();

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  async function deleteRequest() {
    const { data, error, loading } = await SMgraph.mutate({
      mutation: DELETE_REQUEST,
      variables: {
        id: rowData.id,
      },
    });

    if (data.deleteRequest) {
      setOpen(false);
      dispatch({
        type: "ALERT_MESSAGE",
        payload: {
          message: `Successfully deleted Request: ${rowData.requestCode}`,
          translationId: "request.deleted",
          severity: "success",
        },
      });
      refresh();
    }
  }

  return (
    /*
     @prop data-testid: Id to use inside deleterequestbutton.test.js file.
     */
    <div ref={ref} data-testid={"DeleteRequestButtonTestId"}>
      <IconButton title="Delete" color="primary" onClick={handleOpen}>
        <DeleteForever />
      </IconButton>
      <Dialog
        classes={{
          paper: classes.dialog,
        }}
        open={open}
        onClose={handleClose}
      >
        <DialogTitle>
          <Typography className={classes.wrapIcon}>
            <Delete />
            <FormattedMessage
              id={"some.id"}
              defaultMessage={`Delete ${rowData?.requestCode}`}
            />
          </Typography>
        </DialogTitle>
        <DialogContent className={classes.grayBackground}>
          <Typography className={classes.wrapIcon}>
            <Warning style={{ color: "orange" }} />
            <FormattedMessage
              id={"some.id"}
              defaultMessage={`You are about to delete
            ${rowData?.requestCode}. This action will delete the request in the
            list of requests. Click confirm to proceed.`}
            />
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button
            style={{ textTransform: "none" }}
            color="primary"
            onClick={handleClose}
          >
            <FormattedMessage id={"some.id"} defaultMessage={"Cancel"} />
          </Button>
          <Button
            color="primary"
            variant="contained"
            onClick={deleteRequest}
            endIcon={<Send />}
          >
            <FormattedMessage id={"some.id"} defaultMessage={"CONFIRM"} />
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
});
// Type and required properties
DeleteRequestButtonMolecule.propTypes = {};
// Default properties
DeleteRequestButtonMolecule.defaultProps = {};

export default DeleteRequestButtonMolecule;
