import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import { Typography, IconButton } from '@material-ui/core';
//import { IconButton } from "components/atoms/CustomButtons";
import ValidateIcon from '@material-ui/icons/CheckCircle';
import { FormattedMessage } from 'react-intl';
import ConfirmDialogRaw from 'components/atoms/ConfirmDialogRaw';
import { useDispatch } from 'react-redux';
import { SMgraph } from 'utils/apollo';
import { MODIFY_REQUEST } from 'utils/apollo/gql/RequestManager';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const AcceptButtonMolecule = React.forwardRef((props, ref) => {
  const [open, setOpen] = React.useState(false);
  const { refresh, rowData, children, idStatus, ...rest } = props;
  const dispatch = useDispatch();

  const validateRequest = async (query) => {
    //props.action(props.row.original);
    const STATUS_ID_APPROVED = idStatus;
    const response = await SMgraph.mutate({
      mutation: MODIFY_REQUEST,
      variables: {
        RequestTo: {
          id: rowData.id,
          status: { id: STATUS_ID_APPROVED },
        },
      },
    });
    if (response.data) successValidate();
  };

  const warningMessage = () => {
    setOpen(true);
  };

  const successValidate = () => {
    //do other stuff
    setOpen(false);
    refresh();
    dispatch({
      type: 'ALERT_MESSAGE',
      payload: {
        message: 'Request validated',
        translationId: 'request.validated',
        severity: 'success',
      },
    });
  };

  const cancelValidate = () => {
    //do other stuff
    setOpen(false);
  };

  return (
    /* 
     @prop data-testid: Id to use inside acceptbutton.test.js file.
     */
    <div data-testid={'AcceptButtonTestId'}>
      <ConfirmDialogRaw
        open={open}
        title={
          <FormattedMessage
            id={'sm.validatereq.warning.title'}
            defaultMessage={'Are you sure?'}
          />
        }
        onOk={validateRequest}
        onCancel={cancelValidate}
      >
        <Typography variant='body1'>
          <FormattedMessage
            id={'sm.validatereq.warning.message'}
            defaultMessage={'Do you want to confirm?'}
          />
        </Typography>
      </ConfirmDialogRaw>
      {/* use this button to add a edit kind of action */}
      <IconButton
        // id = {`test-validate-${props.row.original.sr_id}`}

        onClick={warningMessage}
        color='primary'
      >
        <ValidateIcon />
      </IconButton>
    </div>
  );
});
// Type and required properties
AcceptButtonMolecule.propTypes = {};
// Default properties
AcceptButtonMolecule.defaultProps = {};

export default AcceptButtonMolecule;
