export const BASEPATH = '/sm'
export const REQUEST_MANAGER = '/requestmanager';
export const CREATE_REQUEST_SERVICE = '/requestmanager/create-request-service';
export const VENDOR = '/vendor';
export const REVIEW = '/review';
export const INSPECT = '/inspect';
export const DESIGN = '/design';
