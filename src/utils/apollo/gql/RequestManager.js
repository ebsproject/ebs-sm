import { gql } from "@apollo/client";

// QUERIES FIND BY ID

// QUERIES FIND LIST
// Find Request List
export const FIND_REQUEST_LIST = gql`
  query findRequestList(
    $page: Int!
    $size: Int!
    $filters: [FilterInput]
    $disjunctionFilters: Boolean
  ) {
    findRequestList(
      page: { number: $page, size: $size }
      filters: $filters
      disjunctionFilters: $disjunctionFilters
      sort: { col: "id", mod: DES }
    ) {
      totalPages
      totalElements
      content {
        id
        requester
        requestCode
        submitionDate
        adminContactName
        completedBy
        icc
        description
        requesterEmail
        adminContactEmail
        listId
        totalEntities
        tenant
        crop {
          cropName
          id
        }
        purpose {
          name
          id
        }
        id
        serviceprovider {
          name
          id
        }
        service {
          name
          id
        }
        servicetype {
          id
          name
        }
        status {
          id
          name
        }
        program{
          programName
          id
        }
      }
    }
  }
`;
// Find Crop List
export const FIND_CROP_LIST = gql`
  query {
    findCropList {
      content {
        cropName
        id
      }
    }
  }
`;

export const FIND_ENTRY_LIST_LIST = gql`
  query {
    findPlantEntryListList {
      content {
        id
        listName
        experiementName
        occurrenceCode
        siteName
        fieldName
        experimentYear
        experimentSeason
        expEntryNumber
        plotCode
        germplasmCode
        pedigree
        seedGeneration
        seedCode
        materialType
        x
        y
        materialClass
        plantNumber
      }
    }
  }
`;

// Find Tyssue Type List
export const FIND_TYSSUE_TYPE_LIST = gql`
  query {
    findTissueTypeList {
      content {
        id
        description
      }
    }
  }
`;
// Find Program List
export const FIND_PROGRAM_LIST = gql`
  query {
    findProgramList {
      content {
        programName
        id
      }
    }
  }
`;
// Find Service Provider List
export const FIND_SERVICE_PROVIDER_LIST = gql`
  query {
    findServiceProviderList(page: { number: 1, size: 100 }) {
      content {
        name
        id
        servicetypes {
          name
          id
          purposes {
            name
            id
            services {
              name
              id
            }
          }
          requests{
            program{
              id
            }
          }
        }
      }
    }
  }
`;
// Find Service Type List
export const FIND_SERVICE_TYPE_LIST = gql`
  query {
    findServiceTypeList {
      content {
        id
        name
      }
    }
  }
`;

export const FIND_SERVICE_TYPE_BY_SERVICE_PROVIDER_ID = gql`
query findServiceProvider($id: ID!) {
  findServiceProvider(id:$id){
    servicetypes{
      id
      name
    } 
  }
}
`;

export const FIND_PURPOSES_BY_SERVICE_TYPE_ID = gql`
query findServiceType($id: ID!) {
  findServiceType(id:$id){
    purposes{
      id
      name
    } 
  }
}
`;

export const FIND_SERVICE_BY_PURPOSE_ID = gql`
query findPurpose($id: ID!) {
  findPurpose(id:$id){
    services{
      id
      name
    } 
  }
}
`;

// Find Purpose List
export const FIND_PURPOSE_LIST = gql`
  query {
    findPurposeList {
      content {
        id
        name
      }
    }
  }
`;
// Find Service List
export const FIND_SERVICE_LIST = gql`
  query {
    findServiceList {
      content {
        id
        name
      }
    }
  }
`;

// MUTATIONS CREATE
// Create Request
export const CREATE_REQUEST = gql`
  mutation createRequest($RequestTo: RequestInput!) {
    createRequest(RequestTo: $RequestTo) {
      id
    }
  }
`;

// MUTATION MODIFY
// Modify Request
export const MODIFY_REQUEST = gql`
  mutation modifyRequest($RequestTo: RequestInput!) {
    modifyRequest(RequestTo: $RequestTo) {
      id
    }
  }
`;

// MUTATION DELETE
// Delete Request
export const DELETE_REQUEST = gql`
  mutation deleteRequest($id: Int!) {
    deleteRequest(idrequest: $id)
  }
`;

export const FIND_BATCH_LIST = gql`
  query findBatchList(
    $page: Int!
    $size: Int!
    $filters: [FilterInput]
  ) {
    findBatchList(
      page: { number: $page, size: $size }
      filters: $filters
      sort: { col: "id", mod: DES }
    ) {
      totalPages
      totalElements
      content {
        id
            name
            objective
            numMembers
            numContainers
            numControls
            batchcode {
              name
              code
              lastPlateNumber
              lastSampleNumber
            }
        
      }
    }
  }
`;

