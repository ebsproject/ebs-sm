import gql from "graphql-tag";

export const FIND_VENDOR_LIST = gql`
  query {
    findVendorList(
      page: { number: 1, size: 10 }
      sort: { col: "id", mod: DES }
    ) {
      totalElements
      totalPages
      content {
        code
        reference
        status
        tenant
        id
        technologyplatform {
          name
        }
        markers {
          name
        }
        dataformat {
          name
        }
        shipments {
          comment
        }
        services {
          name
        }
      }
    }
  }
`;

export const FIND_REQUEST_LIST = gql`
  query {
    findRequestList(
      page: { number: 1, size: 10 }
      sort: { col: "id", mod: DES }
    ) {
      totalPages
      totalElements
      content {
        id
        requester
        requestCode
        submitionDate
        icc
        description
        service {
          name
        }
        status {
          name
        }
        serviceprovider {
          name
        }
        tenant
        totalEntities
        purpose {
          code
        }
      }
    }
  }
`;
