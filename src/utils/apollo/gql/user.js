import gql from 'graphql-tag'

// QUERIES
export const FIND_USER_LIST = gql`
  query findUserList($val: String!) {
    findUserList(filters: { col: "userName", mod: EQ, val: $val }) {
      content {
        userName
        person {
          giveName
          familyName
          additionalName
          email
          gender
          jobTitle
          status
          knowsAbout
          teams {
            teamcode
            teamname
            description
            programs {
              programcode
              programname
              crop {
                id
                cropcode
                cropname
              }
            }
          }
        }
        roles {
          description
          actions {
            action
            description
            workflownode {
              name
            }
          }
        }
      }
    }
  }
`
