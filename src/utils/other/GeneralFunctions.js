import React from "react";
import {
  FormControl,
  MenuItem,
  Select,
  OutlinedInput,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

export const formatDate = (date, type) => {
  let dateFormatted = null;
  let month = null;
  let day = null;
  let hours = null;
  let minutes = null;
  let seconds = null;
  // extract months
  if (date.getMonth() + 1 < 10) {
    month = `0${date.getMonth() + 1}`;
  } else {
    month = `${date.getMonth() + 1}`;
  }
  // extract days
  if (date.getDate() < 10) {
    day = `0${date.getDate()}`;
  } else {
    day = `${date.getDate()}`;
  }
  // extract hours
  if (date.getHours() < 10) {
    hours = `0${date.getHours()}`;
  } else {
    hours = `${date.getHours()}`;
  }
  // extract minutes
  if (date.getMinutes() < 10) {
    minutes = `0${date.getMinutes()}`;
  } else {
    minutes = `${date.getMinutes()}`;
  }
  // extract seconds
  if (date.getSeconds() < 10) {
    seconds = `0${date.getSeconds()}`;
  } else {
    seconds = `${date.getSeconds()}`;
  }
  // return timestamp or a date
  type
    ? (dateFormatted = `${date.getFullYear()}-${month}-${day} ${hours}:${minutes}:${seconds}`)
    : (dateFormatted = `${date.getFullYear()}-${month}-${day}`);
  return dateFormatted;
};

// Return a value for DropDown component and a null for DatePicker component
export const extractCFcodeValue = (customField) => {
  return customField.value ? customField.value : null;
};

// Return a label for DropDown component and a date formatted for DatePicker component
export const extractCFtextValue = (customField) => {
  let cfTextValue = null;
  if (typeof customField === "string") {
    cfTextValue = customField;
  } else {
    cfTextValue = customField.label
      ? customField.label
      : formatDate(customField, true);
  }
  return cfTextValue;
};

// Extract custom fields values of a Request already created
export const extractCFValues = (cfValues) => {
  // Save an object with each CFValue
  let CFValues = {};
  cfValues.map((cf) => {
    CFValues[cf.workflownodecf.name] =
      cf.codevalue > 0 ? cf.codevalue : cf.textvalue;
  });
  return CFValues;
};

// Custom Filter for EBS_GRID_LIBRARY
export function SelectColumnFilter({
  column: { filterValue, setFilter, preFilteredRows, id },
}) {
  const useStyles = makeStyles((theme) => ({
    formControl: {
      minWidth: 120,
    },
    input: {
      padding: "10px 14px",
    },
  }));
  const classes = useStyles();
  // Calculate the options for filtering
  // using the preFilteredRows
  const options = React.useMemo(() => {
    const selectOptions = [];
    preFilteredRows.forEach((row) => {
      // prevents duplicates before pushing in the array
      if (!selectOptions.includes(row.values[id]))
        selectOptions.push(row.values[id]);
    });
    return [...selectOptions];
  }, [id, preFilteredRows]);

  // Render a multi-select box
  return (
    <FormControl variant="outlined" className={classes.formControl}>
      <Select
        value={filterValue || ""}
        onChange={(e) => {
          setFilter(e.target.value);
        }}
        input={<OutlinedInput classes={{ input: classes.input }} />}
        displayEmpty
      >
        <MenuItem value="">All</MenuItem>
        {options.map((option, i) => ( 
          <MenuItem key={i} value={option}>
            {option}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}
