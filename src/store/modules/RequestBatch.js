import { CSrest } from 'utils/axios';
import { updateStatusRequest } from 'utils/apollo/CSclient';
import {
  createBatchWithRequest,
  getBatchList,
  getBatchCodeById,
  updateListRequestByBatchByID,
} from 'utils/apollo/SMclient';

export const initialState = {
  batchName: '',
  isLoading: false,
  data: [],
  error: null,
};

export const BATCH_NAME = 'Request/BATCH_NAME';
export const START_BATCH = 'Batch/START_BATCH';
export const LOAD_BATCH = 'Batch/LOAD_BATCH';
export const BATCH_SUCCESS = 'Batch/BATCH_SUCCESS';
export const NEXT_STAGE = 'Batch/ACCEPT_NEXTSTAGE';
export const REJECTED_STAGE = 'Batch/REJECTED_STAGE';

export const nextStage = (row) => ({
  type: NEXT_STAGE,
  row,
});

export const rejectStage = (row) => ({
  type: REJECTED_STAGE,
  row,
});

export const setBatchName = (batchName) => ({
  type: BATCH_NAME,
  batchName,
});

export const startBatch = () => ({
  type: START_BATCH,
});

export const loadBatch = (payload) => ({
  type: LOAD_BATCH,
  payload,
});
export const batchSuccess = () => ({
  type: BATCH_SUCCESS,
});

export const nextStageBatch = (row) => async (dispatch) => {
  updateListRequestByBatchByID(row, 21, true);
  dispatch(nextStage(row));
};

export const rejectedSatge = (row) => async (dispatch) => {
  updateListRequestByBatchByID(row, 18, false);
  dispatch(rejectStage(row));
};
export const updateBatchName = (requestMap, idBatchCode) => async (dispatch) => {
  let intServiceCode = 0;
  let intPurpose = 0;
  let intArrProgram = [];
  var ProgramMapa = new Map();

  for (let [key, value] of requestMap) {
    ProgramMapa.set(value.ProgramCode_Id, value.ProgramCode_Id);
    intPurpose = value.PurposeCode_Id;
    intServiceCode = value.ServiceCode_Id;
  }
  let keys = Array.from(ProgramMapa.keys());
  var URL = '';
  //if (requestMap.size ===1)
  //URL= '/tenant/1/rules/4';
  //else
  console.log('keys', keys);
  console.log('intPurpose', intPurpose);
  console.log('intServiceCode', intServiceCode);
  URL = '/tenant/1/rules/3';
  await CSrest.post(URL, {
    servicecode: intServiceCode,
    program: keys,
    purpose: intPurpose,
  })
    .then((response) => {
      let code = response.data.code;
      let last_batch_number = 0;
      new Promise(() => {
        getBatchCodeById(1).then((response) => {
          last_batch_number = response.lastbatch_number + 1;
          dispatch(setBatchName(code + last_batch_number));
        });
      });
    })
    .catch((error) => {
      console.log('error... ', error);
      throw new Error(error.message);
    });
};

export const batchRequest =
  (requestMap, batchName, description, idBatchCode) => async (dispatch) => {
    let total = 0;
    for (let [key, value] of requestMap) {
      new Promise((resolve, reject) => {
        updateStatusRequest(value, 20, 16);
        total += parseInt(value.Total, 10);
      });
    }
    createBatchWithRequest(
      requestMap,
      batchName,
      description,
      1,
      2,
      total,
      1,
      idBatchCode,
    );
    //getBatchList(4, "false");
  };

export const getBatchListResult =
  (idServiceProvider) => async (dispatch, getState) => {
    dispatch(startBatch());
    try {
      var resolver = await getBatchList(idServiceProvider, 'false');
      dispatch(loadBatch(resolver));
      dispatch(batchSuccess());
    } catch (error) {
      throw new Error(error.message);
    }
  };

export default function RequestReducer(
  state = initialState,
  { type, payload, batchName, row },
) {
  switch (type) {
    case BATCH_NAME:
      return {
        ...state,
        batchName,
      };
    case START_BATCH:
      return {
        ...state,
        isLoading: true,
      };
    case BATCH_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null,
      };
    case LOAD_BATCH:
      return {
        ...state,
        data: payload,
        isLoading: false,
        error: null,
      };
    case NEXT_STAGE: {
      let newDataValid = [...state.data];
      newDataValid = newDataValid.filter((e) => row.id !== e.id);
      return {
        ...state,
        data: newDataValid,
      };
    }
    case REJECTED_STAGE: {
      let newDataReject = [...state.data];
      newDataReject = newDataReject.filter((e) => row.id !== e.id);
      return {
        ...state,
        data: newDataReject,
      };
    }
    default:
      return state;
  }
}
