import React, { useState } from 'react';
import { Provider } from 'react-redux';
import { ApolloProvider } from '@apollo/client';
import { BrowserRouter, Route } from 'react-router-dom';
import { EBSMaterialUIProvider } from '@ebs/styleguide';
import { InstanceContext, InstanceProvider } from 'cs-ui';
import { IntlProvider } from 'react-intl';
import store from './store';
import { SMgraph } from 'utils/apollo';
// views
import RequestManager from 'pages/RequestManager';
import CreateRequestService from 'pages/RequestManager/pages/CreateRequestService';
import SampleManager from 'pages/SampleManager';
import RequestReview from 'pages/RequestReview';
import BatchView from 'pages/InspectView';
import DesignView from 'pages/DesignView';
import {
  BASEPATH,
  REQUEST_MANAGER,
  CREATE_REQUEST_SERVICE,
  VENDOR,
  REVIEW,
  INSPECT,
  DESIGN,
} from './Routes';

export default function App() {
  const [locale, setLocale] = useState('en');
  const [messages, setMessages] = useState(null);

  return (
    <EBSMaterialUIProvider>
      <Provider store={store}>
        <ApolloProvider client={SMgraph}>
          <IntlProvider locale={locale} messages={messages} defaultLocale='en'>
            <BrowserRouter>
              <Route
                path={BASEPATH}
                render={({ match }) => (
                  <>
                    <Route
                      exact
                      path={match.url + REQUEST_MANAGER}
                      component={RequestManager}
                    />
                    <Route
                      exact
                      path={match.url + CREATE_REQUEST_SERVICE}
                      component={CreateRequestService}
                    />
                    <Route
                      exact
                      path={match.url + VENDOR}
                      component={SampleManager}
                    />
                    <Route
                      exact
                      path={match.url + REVIEW}
                      component={RequestReview}
                    />
                    <Route exact path={match.url + INSPECT} component={BatchView} />
                    <Route exact path={match.url + DESIGN} component={DesignView} />
                  </>
                )}
              />
            </BrowserRouter>
          </IntlProvider>
        </ApolloProvider>
      </Provider>
    </EBSMaterialUIProvider>
  );
}
