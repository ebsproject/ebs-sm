import React from 'react'
import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import RequestReviewGrid from "components/organisms/RequestReviewGrid";

export default function RequestReviewView(props) {
  
  /*
  @prop data-testid: Id to use inside requestreview.test.js file.
 */
  return (
    <Container 
    data-testid={'RequestReviewTestId'} 
    component='main'
    maxWidth="xl"
    disableGutters
    fixed
    >
      
    <RequestReviewGrid/>
    </Container>
  )
}
// Type and required properties
RequestReviewView.propTypes = {}
// Default properties
RequestReviewView.defaultProps = {}
