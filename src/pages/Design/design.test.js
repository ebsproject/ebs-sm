import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import Design from './designview'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
const getBatchListResult = () =>{

}
const props = {
  row:{
    original:{
      id:1,
      actionNext:'actionNext',
      actionReject:'actionReject',
    },
  },
  data:[],

}

afterEach(cleanup)
test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Design {...props} getBatchListResult={getBatchListResult}></Design>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<Design {...props} getBatchListResult={getBatchListResult}></Design>)
  expect(getByTestId('DesignTestId')).toBeInTheDocument()
})
