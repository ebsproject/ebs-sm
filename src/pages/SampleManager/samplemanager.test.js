import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import SampleManager from './samplemanager'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<SampleManager></SampleManager>, div)
})

// Props to send component to be rendered
const props = {}

test('Render correctly', () => {
  const { getByTestId } = render(<SampleManager {...props}></SampleManager>)
  expect(getByTestId('SampleManagerTestId')).toBeInTheDocument()
})
