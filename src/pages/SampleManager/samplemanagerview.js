import React from "react";
import PropTypes from "prop-types";
import Container from "@material-ui/core/Container";
import AlertNotification from "components/atoms/AlertNotification";
import VendorOrder from "components/organisms/VendorOrder";

export default function SampleManagerView(props) {
  /*
  @prop data-testid: Id to use inside samplemanager.test.js file.
 */
  return (
    <Container
      data-testid={"SampleManagerTestId"}
      component="div"
      maxWidth="xl"
      disableGutters
      fixed
    >
      <AlertNotification />
      <VendorOrder />
    </Container>
  );
}
// Type and required properties
SampleManagerView.propTypes = {};
// Default properties
SampleManagerView.defaultProps = {};
