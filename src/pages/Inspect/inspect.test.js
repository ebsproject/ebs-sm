import React from 'react'
import ReactDOM from 'react-dom'
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import configureStore from "redux-mock-store";
import thunk from "redux-thunk"
// Component to be Test
import Inspect from './inspectview'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

configure({ adapter: new Adapter() });

const getBatchListResult = () =>{

}
const props = {
  row:{
    original:{
      id:1,
      actionNext:'actionNext',
      actionReject:'actionReject',
    },
  },
  data:[],
  getBatchListResult:'getBatchListResult',

}

afterEach(cleanup)
test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Inspect {...props} getBatchListResult={getBatchListResult}></Inspect>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<Inspect {...props} getBatchListResult={getBatchListResult}></Inspect>)
  expect(getByTestId('InspectTestId')).toBeInTheDocument()
})

describe("MockUseEffect ", () => {
  it("should render RecipeItem components with data empty and mock useEffect", () => {
    const { getByTestId } = render(<Inspect {...props } getBatchListResult={getBatchListResult}></Inspect>)
    expect(getByTestId('InspectTestId')).toBeInTheDocument()
  });
});