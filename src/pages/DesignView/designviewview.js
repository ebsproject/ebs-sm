import React,  { useState } from 'react'
import PropTypes from 'prop-types'
import Container from '@material-ui/core/Container'
import DesignVIewGrid from "components/organisms/DesignViewGrid";


export default function DesignViewView(props) {
  const { ...rest } = props

  return (
    <Container data-testid={'DesignViewTestId'} component='main'
    maxWidth="xl"
    disableGutters
    fixed>
    <DesignVIewGrid/>
    </Container>
  )
}
// Type and required properties
DesignViewView.propTypes = {}
// Default properties
DesignViewView.defaultProps = {}
