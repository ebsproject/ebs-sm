import React from 'react'
import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import BatchVIewGrid from "components/organisms/BatchVIewGrid";

export default function InspectViewView(props) {
  // Props
  const { ...rest } = props

  /*
  @prop data-testid: Id to use inside inspectview.test.js file.
 */
  return (
    <Container 
    data-testid={'InspectReviewTestId'} 
    component='main'
    maxWidth="xl"
    disableGutters
    fixed
    >
      
    <BatchVIewGrid/>
    </Container>
  )
}
// Type and required properties
InspectViewView.propTypes = {}
// Default properties
InspectViewView.defaultProps = {}
