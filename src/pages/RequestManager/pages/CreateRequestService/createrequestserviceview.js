import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { CSrest, SMrest } from 'utils/axios';
import { SMgraph } from 'utils/apollo';
import { CREATE_REQUEST } from 'utils/apollo/gql/RequestManager';
import BasicTab from 'components/atoms/CreateRequestServiceTabPanels/Basic/BasicTab';
import ServiceProviderTab from 'components/atoms/CreateRequestServiceTabPanels/Basic/ServiceProviderTab';
import EntryListTab from 'components/atoms/CreateRequestServiceTabPanels/EntryList/EntryListTab';
import ReviewTab from 'components/atoms/CreateRequestServiceTabPanels/Review/ReviewTab';
import { formatDate } from 'utils/other/generalFunctions';
import { useDispatch } from 'react-redux';
import {
  Box,
  Button,
  Container,
  Grid,
  Paper,
  Tab,
  Tabs,
  Toolbar,
  Typography,
  makeStyles,
} from '@material-ui/core';
import ConfirmDialogRawAtom from 'components/atoms/ConfirmDialogRaw';

const useStyles = makeStyles((theme) => ({
  tabNumber: {
    padding: '3px 8px',
    backgroundColor: theme.palette.primary.main,
    borderRadius: '50px',
    color: 'white',
    fontWeight: '600',
    fontSize: '14px',
    marginRight: '5px',
  },
  title: {
    flexGrow: 1,
  },
  tabStyle: {
    cursor: 'default',
    pointerEvents: 'none',
  },
  navigationButtonsStyle: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
}));

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

function transformDate(inputDate) {
  return formatDate(inputDate);
}

function formattedTab(classes, tabCount, label, id) {
  return (
    <Typography>
      <span className={classes.tabNumber}>{tabCount}</span>
      <FormattedMessage id={id} defaultMessage={label} />
    </Typography>
  );
}

function TabPanel(props) {
  const { children, value, index } = props;

  return (
    <div role='tabpanel' hidden={value !== index}>
      {value === index && <>{children}</>}
    </div>
  );
}

export default function CreateRequestServiceView(props) {
  // Props
  const { ...rest } = props;
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const [serviceProvider, setServiceProvider] = useState(null);
  const [cimmyt, setCimmyt] = useState(null);
  const [irri, setIrri] = useState(null);
  const [requesterEmail, setrequesterEmail] = React.useState(null);
  const [requester, setRequester] = React.useState(null);
  const [requestCode, setRequestCode] = React.useState('');

  const [listDbId, setListDbId] = useState(null);
  const [totalNumberOfEntries, setTotalNumberOfEntries] = useState(null);
  const [listName, setListName] = useState(null);

  // State to save form data
  const [general, setGeneral] = useState(null);
  const [serviceProviderForm, setServiceProviderForm] = useState({});
  const [basicInformationOfServiceForm, setBasicInformationOfServiceForm] =
    useState(null);

  const dispatch = useDispatch();
  const handleChange = (value) => {
    setValue(value);
  };

  const getEntryTabData = (checked, listDbId, totalNumberOfEntries, listName) => {
    if (checked) {
      setListDbId(listDbId);
      setTotalNumberOfEntries(totalNumberOfEntries);
      setListName(listName);
    } else {
      setListDbId(null);
      setTotalNumberOfEntries(null);
    }
  };

  if (!requester && !requesterEmail) {
    // TODO: Remove this next 2 lines. For testing purposes only.
    setrequesterEmail('bims.irri@gmail.com');
    setRequester('Bims, Irri');
    let user = Object.entries(localStorage).map(([key, valueJSON]) => {
      const value = JSON.parse(valueJSON);
      setrequesterEmail(value.userName);
      setRequester('Bims, Irri');
    });
  }

  async function getRequestCode() {
    const { data, error, loading } = await SMrest.get('request/createRequestCode');
    setRequestCode(data.result);
  }

  async function onSubmitCimmyt(formData) {
    formData.requester = requester;
    formData.requesterEmail = requesterEmail;

    getRequestCode();
    setBasicInformationOfServiceForm(formData);
    setCimmyt(formData);
    setIrri(formData);
    handleChange(value + 1);
  }

  async function onSubmitGeneral() {
    let submitValues = {
      id: 0,
      requester: cimmyt.requester,
      requestCode: requestCode,
      submitionDate: transformDate(
        serviceProviderForm.serviceprovider.label.includes('CIMMYT')
          ? basicInformationOfServiceForm.submitionDate
          : new Date(),
      ),
      adminContactName: serviceProviderForm.serviceprovider.label.includes('CIMMYT')
        ? basicInformationOfServiceForm.adminContactName
        : basicInformationOfServiceForm.researcher,
      completedBy: transformDate(basicInformationOfServiceForm.completedBy),
      icc: serviceProviderForm.serviceprovider.label.includes('CIMMYT')
        ? basicInformationOfServiceForm.icc
        : basicInformationOfServiceForm.ocsnumber,
      description: basicInformationOfServiceForm.description,
      requesterEmail: requesterEmail,
      adminContactEmail: serviceProviderForm.serviceprovider.label.includes('CIMMYT')
        ? basicInformationOfServiceForm.adminContactEmail
        : basicInformationOfServiceForm.contactperson,
      listId: 1,
      totalEntities: 94,
      tenant: 1,
      crop: { id: 2 },
      program: { id: 102 },
      purpose: { id: Number(serviceProviderForm.purpose.value) },
      serviceprovider: {
        id: Number(serviceProviderForm.serviceprovider.value),
      },
      status: { id: 1 },
      servicetype: { id: Number(serviceProviderForm.serviceType.value) },
      service: { id: Number(serviceProviderForm.service.value) },
    };

    console.log(submitValues);
    // const { data, error, loading } = await SMgraph.mutate({
    //   mutation: CREATE_REQUEST,
    //   variables: {
    //     RequestTo: {
    //       id: 0,
    //       requester: cimmyt.requester,
    //       requestCode: requestCode,
    //       submitionDate: transformDate(serviceProviderForm.serviceprovider.label.includes('CIMMYT') ? basicInformationOfServiceForm.submitionDate: new Date()),
    //       adminContactName: serviceProviderForm.serviceprovider.label.includes('CIMMYT') ? basicInformationOfServiceForm.adminContactName : basicInformationOfServiceForm.researcher,
    //       completedBy: transformDate(basicInformationOfServiceForm.completedBy),
    //       icc: serviceProviderForm.serviceprovider.label.includes('CIMMYT') ? basicInformationOfServiceForm.icc : basicInformationOfServiceForm.ocsnumber,
    //       description: basicInformationOfServiceForm.description,
    //       requesterEmail: requesterEmail,
    //       adminContactEmail: serviceProviderForm.serviceprovider.label.includes('CIMMYT') ? basicInformationOfServiceForm.adminContactEmail : basicInformationOfServiceForm.contactperson,
    //       listId: 1,
    //       totalEntities: 94,
    //       tenant: 1,
    //       crop: { id: 2 },
    //       program: { id: 102 },
    //       purpose: { id: Number(serviceProviderForm.purpose.value) },
    //       serviceprovider: {
    //         id: Number(serviceProviderForm.serviceprovider.value),
    //       },
    //       status: { id: 1 },
    //       servicetype: { id: Number(serviceProviderForm.serviceType.value) },
    //       service: { id: Number(serviceProviderForm.service.value) },
    //     },
    //   },
    // });
    // if(data.createRequest) {
    //   alert("successful!")
    //   dispatch({
    //     type: "ALERT_MESSAGE",
    //     payload: {
    //       message: "Request created",
    //       translationId: "request.created",
    //       severity: "success",
    //     },
    //   });
    // }
  }

  /*
  @prop data-testid: Id to use inside createrequestservice.test.js file.
 */
  return (
    <Container data-testid={'CreateRequestServiceTestId'} component='main'>
      <Toolbar>
        <Typography variant='h6' className={classes.title}>
          <FormattedMessage
            id={'some.id'}
            defaultMessage={'Create Request Service'}
          />
        </Typography>
      </Toolbar>

      <Grid container>
        <Grid item xs={12} md={8}>
          <Paper square>
            <Tabs value={value}>
              <Tab
                className={classes.tabStyle}
                label={formattedTab(classes, '0', 'ENTRY LIST', 'some.id')}
                {...a11yProps(0)}
              />
              <Tab
                className={classes.tabStyle}
                label={formattedTab(classes, '1', 'BASIC', 'some.id')}
                {...a11yProps(1)}
              />
              <Tab
                className={classes.tabStyle}
                label={formattedTab(classes, '2', 'SERVICES', 'some.id')}
                {...a11yProps(2)}
              />
              <Tab
                className={classes.tabStyle}
                label={formattedTab(classes, '3', 'REVIEW', 'some.id')}
                {...a11yProps(3)}
              />
            </Tabs>
          </Paper>
        </Grid>

        <Grid item xs={12} md={11}>
          <Toolbar className={classes.navigationButtonsStyle}>
            <Box pr={2}>
              <Button onClick={() => window.location.reload()}>
                <FormattedMessage id={'some.id'} defaultMessage={'CANCEL'} />
              </Button>
            </Box>
            {value !== 0 && (
              <>
                <Box pr={2}>
                  <Button
                    color='primary'
                    variant='contained'
                    onClick={() => handleChange(value - 1)}
                  >
                    <FormattedMessage id={'some.id'} defaultMessage={'BACK'} />
                  </Button>
                </Box>
              </>
            )}
            {(value === 0 || value === 3) && (
              <Button
                color='primary'
                variant='contained'
                onClick={
                  value !== 2
                    ? () => handleChange(value + 1)
                    : () => onSubmitGeneral()
                }
              >
                {value === 0 && (
                  <FormattedMessage id={'some.id'} defaultMessage={'Next'} />
                )}

                {value === 3 && (
                  <FormattedMessage id={'some.id'} defaultMessage={'Submit'} />
                )}
              </Button>
            )}
          </Toolbar>
        </Grid>
      </Grid>

      <TabPanel value={value} index={0}>
        <EntryListTab getEntryTabData={getEntryTabData} />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <BasicTab
          serviceProviderForm={serviceProviderForm}
          setRequester={setRequester}
          value={value}
          handleChange={handleChange}
          serviceProvider={serviceProvider}
          onSubmitCimmyt={onSubmitCimmyt}
          cimmyt={cimmyt}
          irri={irri}
          requester={requester}
          requesterEmail={requesterEmail}
        />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <ServiceProviderTab
          serviceProviderForm={serviceProviderForm}
          value={value}
          handleChange={handleChange}
          serviceProvider={serviceProvider}
          cimmyt={cimmyt}
          irri={irri}
        />
      </TabPanel>
      <TabPanel value={value} index={3}>
        <ReviewTab
          requestCode={requestCode}
          listDbId={listDbId}
          basicTabData1={serviceProviderForm}
          basicTabData2={basicInformationOfServiceForm}
          basicInformationOfServiceForm={basicInformationOfServiceForm}
          listDbId={listDbId}
          totalNumberOfEntries={totalNumberOfEntries}
          listName={listName}
        />
      </TabPanel>
    </Container>
  );
}
// Type and required properties
CreateRequestServiceView.propTypes = {};
// Default properties
CreateRequestServiceView.defaultProps = {};
