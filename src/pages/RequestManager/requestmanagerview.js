import React from 'react';
import PropTypes from 'prop-types';
import RequestStatusGrid from 'components/organisms/RequestStatusGrid';
import AlertNotification from 'components/atoms/AlertNotification';

export default function RequestManagerView(props) {
  /*
  @prop data-testid: Id to use inside requestmanager.test.js file.
 */
  return (
    <div data-testid={'RequestManagerTestId'}>
      <AlertNotification />
      <RequestStatusGrid />
    </div>
  );
}
// Type and required properties
RequestManagerView.propTypes = {};
// Default properties
RequestManagerView.defaultProps = {};
