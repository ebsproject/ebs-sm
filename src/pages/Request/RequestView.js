import React, { Component } from "react";
// time
import moment from "moment";
// react component for creating dynamic tables
import "react-table/react-table.css";
import "assets/scss/material-dashboard-pro-react/plugins/_plugin-react-table.css";
import ReactTable from "react-table";
import checkboxHOC from "react-table/lib/hoc/selectTable";
// core components
import GridContainer from "components/molecules/Grid/GridContainer.js";
import GridItem from "components/molecules/Grid/GridItem.js";
import Card from "components/molecules/Card/Card.js";
import CardBody from "components/molecules/Card/CardBody.js";
import CardHeader from "components/molecules/Card/CardHeader.js";
import { FormattedMessage } from 'react-intl'
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
// other components
import RequestBatch from "components/organisms/RequestBatch";
import StatusBadges from "components/molecules/StatusBadges/StatusBadges";
import StatusActions from "components/molecules/StatusActions/StatusActions";
import Pagination from "components/molecules/Pagination/Pagination"
import { isPunctuatorTokenKind } from "graphql/language/lexer";

// import Authentication from "components/atoms/Authentication";

//HOC for checkboxes
const CheckboxTable = checkboxHOC(ReactTable);

export default class RequestTable extends Component {
    constructor(props) {
        super(props)
        this.state = { data_l: [], columns_l : [] };
        this.checkboxTable = React.createRef()
    }

    componentDidMount() {
        const { getRequest, getUser } = this.props;
        getUser();
        getRequest();
    }

    UNSAFE_componentWillReceiveProps (nextProps) {
        if (nextProps.data.length > 0 && this.props.data !== nextProps.data) {
            const dataActions = this.getDataActions(nextProps.data)
            this.setState({
                data_l: dataActions, 
                columns_l: this.getColumns(dataActions[0])
            });
        }
    }

    toggleSelection = (key, shift, row) => {
        let selection = [...this.props.selection];
        const keyIndex = selection.indexOf(row.id);
        let mapSelection = this.props.mapSelection
        // check to see if the key exists
        if (keyIndex >= 0) {
            // it does exist so we will remove it using destructing
            selection = [
                ...selection.slice(0, keyIndex),
                ...selection.slice(keyIndex + 1)
            ];
            if (mapSelection.has(row.id)){
                mapSelection.delete(row.id)
            }
        } else {
            
            selection.push(row.id);
            mapSelection.set(row.id,row)
            
        }
        if (row.status === "Approved" )
        this.props.addSelection(selection);
    };



    toggleAll = () => {
        const selectAll = this.props.selectAll ? false : true;
        const selection = [];
        let mapSelection = this.props.mapSelection;
        if (selectAll) {
            // we need to get at the internals of ReactTable
            const wrappedInstance = this.checkboxTable.getWrappedInstance();
            // the 'sortedData' property contains the currently accessible records based on the filter and sort
            const currentRecords = wrappedInstance.getResolvedState().sortedData;
            // we just push all the IDs onto the selection array
            currentRecords.forEach(item => {
                if (item._original.status === "Approved"){
                    selection.push(item._original.id);
                    mapSelection.set(item._original.id,item);
                }
            });
        }

        this.props.addSelectionAll(selectAll, selection);
    };

    toggleColumnChooser = (value) => {
        let index = value -1;
        this.setState(
            prevState => {
                const columns1 = [];
                columns1.push(...this.state.columns_l);
                columns1[index].show = !columns1[index].show;
                if (columns1[index].columns) {
                columns1[index].columns.forEach(item => {
                    item.show = !item.show
                })
                }

                return {
                    columns: columns1,
                };
            }, () => {
            console.log(this.state.columns_l)
            }
        );
    };

    isSelected = (key) => {
        /*
        Instead of passing our external selection state we provide an 'isSelected'
        callback and detect the selection state ourselves. This allows any implementation
        for selection (either an array, object keys, or even a Javascript Set object).
        */

        return this.props.selection.includes(key);
    };

    logSelection = () => {
        console.log("selection:", this.props.selection);
    }

    /*
       Define rules apply for each row per column
       We have row actions null but here we put what actions must be there
    */
    getColumns = (sample) => {
        // const ignoreColumns = ['phone1','phone2','web','email','_id'];
        const columns = [];
        Object.keys(sample).forEach(key => {
            // if (ignoreColumns.includes(key)) continue;
            let confColumns = {}
            if (key !== "_id") {
                if (key === "id" || key === "RM_Listid"  
                || key === "Crop_ID" 
                || key === "ProgramCode_Id"
                || key === "Purpose_Id"
                || key === "Service_Id"
                || key === "TissueType_Id"
                || key === "Crop_ID"
                || key === "ServiceProvider_Id"
                || key === "PurposeCode_Id"
                || key === "ServiceCode_Id"
                ) {
                    confColumns = {
                        minWidth: 60,
                        width: 80,
                        maxWidth: 100,
                        style: {
                            textAlign: "center",
                        },
                        show:false
                    }
                }
                if (key === "Service_Provider" || key === "requester") {
                    confColumns = {
                        minWidth: 80,
                        width: 100,
                        maxWidth: 120,
                    }
                }
                /*Important: We fill here actions row with this values*/
                if (key === "actions") {
                    confColumns = {
                        sortable: false,
                        filterable: false,

                        Cell: row => <StatusActions
                            row={row}
                            valid={this.props.validateRequest}
                            info={this.props.informateRequest}
                            reject={this.props.rejectedRequest}
                        />,
                        minWidth: 120,
                        width: 150,
                        maxWidth: 160,
                    }
                }
                if (key === "service") {
                    confColumns = {
                        minWidth: 80,
                        width: 120,
                        maxWidth: 160,
                    }
                }
                if (key === "status") {
                    confColumns = {
                        Cell: ({ value }) => <StatusBadges value={value} />,
                        minWidth: 100,
                        width: 120,
                    }
                }

                if (key === "date_submitted") {
                    // necessary to sort by date
                    confColumns = {
                        id: key,
                        accessor: ({ date_submitted }) => moment(date_submitted, "YYYY-MM-DD"),
                        Cell: ({ value }) => <span>{moment(value, "DD-MM-YYYY").format("DD-MM-YYYY")}</span>,
                        minWidth: 90,
                        width: 110,
                    }
                }

                if (key === "complete_by") {
                    // necessary to sort by date
                    confColumns = {
                        id: key,
                        accessor: ({ complete_by }) => moment(complete_by, "YYYY-MM-DD"),
                        Cell: ({ value }) => <span>{moment(value, "DD-MM-YYYY").format("DD-MM-YYYY")}</span>,
                        minWidth: 90,
                        width: 110,
                    }
                }

                if (key === "status") {
                    confColumns = {
                        ...confColumns,
                        id: "status",
                        filterMethod: (filter, row) => {

                            if (filter.value === "all") {
                                return true;
                            }
                            if (filter.value === "New") {
                                return row[filter.id] === "New";
                            }
                            if (filter.value === "Approved") {
                                return row[filter.id] === "Approved";
                            }
                            if (filter.value === "Rejected") {
                                return row[filter.id] === "Rejected";
                            }
                            if (filter.value === "Create Design") {
                                return row[filter.id] === "Create Design";
                            }

                        
                        },
                       
                        Filter: ({ filter, onChange }) =>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={filter ? filter.value : "all"}
                                
                                onChange={event => onChange(event.target.value)}
                            >
                                <MenuItem value="all"><FormattedMessage id={'sm.req.tbl.filter.all'} defaultMessage={"Show All"} /></MenuItem>
                                <MenuItem value="New"><FormattedMessage id={'sm.req.tbl.filter.new'} defaultMessage={"New"} /></MenuItem>
                                <MenuItem value="Approved"><FormattedMessage id={'sm.req.tbl.filter.approved'} defaultMessage={"Approved"} /></MenuItem>
                                <MenuItem value="Rejected"><FormattedMessage id={'sm.req.tbl.filter.rejected'} defaultMessage={"Rejected"} /></MenuItem>
                                <MenuItem value="Batch"><FormattedMessage id={'sm.req.tbl.filter.rejected'} defaultMessage={"Batch"} /></MenuItem>
                                <MenuItem value="Create Design"><FormattedMessage id={'sm.req.tbl.filter.rejected'} defaultMessage={"Design"} /></MenuItem>
                               
                            </Select>
                    }
                }

                let header = '';
                switch (key) {
                    case 'Request_Code':
                        header = <FormattedMessage id={'sm.req.tbl.hdr.srid'} defaultMessage={'Request'} />
                        break;
                    case 'actions':
                        header = <FormattedMessage id={'sm.req.tbl.hdr.act'} defaultMessage={'Actions'} />
                        break;
                    case 'sr_type':
                        header = <FormattedMessage id={'sm.req.tbl.hdr.type'} defaultMessage={'SR Type'} />
                        break;
                    case 'Crop':
                        header = <FormattedMessage id={'sm.req.tbl.hdr.crop'} defaultMessage={'Crop'} />
                        break;
                    case 'status':
                        header = <FormattedMessage id={'sm.req.tbl.hdr.status'} defaultMessage={'Status'} />
                        break;
                    case 'Total':
                        header = <FormattedMessage id={'sm.req.tbl.hdr.quantity'} defaultMessage={'Total'} />
                        break;
                    case 'Service_Provider':
                        header = <FormattedMessage id={'sm.req.tbl.hdr.servprov'} defaultMessage={'Service Provider'} />
                        break;
                    case 'Tissue_Type':
                        header = <FormattedMessage id={'sm.req.tbl.hdr.entities'} defaultMessage={'Tissue Type'} />
                        break;
                    case 'service':
                        header = <FormattedMessage id={'sm.req.tbl.hdr.service'} defaultMessage={'Service'} />
                        break;
                    case 'Requester':
                        header = <FormattedMessage id={'sm.req.tbl.hdr.requester'} defaultMessage={'Requester'} />
                        break;
                    case 'Submition_Date':
                        header = <FormattedMessage id={'sm.req.tbl.hdr.subdate'} defaultMessage={'Submit Date'} />
                        break;
                    case 'Complete_by':
                        header = <FormattedMessage id={'sm.req.tbl.hdr.completeby'} defaultMessage={'Complete By'} />
                        break;
                    case 'ICC_Code':
                        header = <FormattedMessage id={'sm.req.tbl.hdr.icccode'} defaultMessage={'ICC'} />
                        break;
                    case 'Program':
                        header = <FormattedMessage id={'sm.req.tbl.hdr.icccode'} defaultMessage={'Program'} />
                        break;
                    case 'Purpose':
                            header = <FormattedMessage id={'sm.req.tbl.hdr.icccode'} defaultMessage={'Purpose'} />
                            break;
                    default:
                        break;
                }

                columns.push({
                    accessor: key,
                    id: key,
                    Header: header || key,
                    ...confColumns,
                    
                });
            }
        });

        return columns;
    }

    getDataActions = (testData) => {
        const dataActions = testData.map(item => {
            const actions = null;
            return {
                _id: item.id,
                actions,
                ...item,
            };
        });

        return dataActions;
    }


    getTrProps = (state, rowInfo, column, instance) => {
        // Maybe I wasn't clear. getTrProps is used inside the code in a number of places and, in at least one of those places, rowInfo is passed as undefined. So, if you are going to use the data from rowInfo you have to check it first. If it is undefined, then just return {} otherwise you should be fine with the return statement you have - assuming your row has a status element on it (note: row is the displayed row and original is your original data - which will have everything, not just the data you are displaying).
        // So, there is nothing wrong with your concept. You just have to ensure that rowInfo is defined before trying to acecss it. e.g.
        // if(rowInfo) /* your return here */
        // else return {}
        if (rowInfo) {
            const selected = this.isSelected(rowInfo.original.id);

            //     return {
            //       onMouseEnter: e =>
            //         console.log("Cell - onMouseEnter", {
            //           state,
            //           rowInfo,
            //           column,
            //           instance,
            //           event: e
            //         })
            //     }

            if (rowInfo.original.status === "Approved") {
                return {
                    style: {
                        backgroundColor: selected ? "lightgreen" : "inherit"
                    }
                }
            }
        }
        return {};
    }


    render() {
        const { toggleSelection, toggleAll, isSelected, getTrProps } = this;
        // const { user, data, isLoading, selection, selectAll } = this.props;
        const { data, isLoading, selection, selectAll, mapSelection, nextStage } = this.props;

        // default initial state
        if(!data.length && !isLoading) return null

        const checkboxProps = {
            selectAll,
            isSelected,
            toggleSelection,
            toggleAll,
            selectType: "checkbox",
            getTrProps
        };

        return (
            <>

                <GridContainer>
                    <GridItem xs={12}>
                        <Card>
                            <CardHeader>
                                <RequestBatch requests={selection} requestMap={mapSelection} nextStage={nextStage} />
                            </CardHeader>
                            <CardBody>
                                <CheckboxTable
                                    noDataText="Loading..."
                                    ref={r => (this.checkboxTable = r)}
                                    filterable
                                    PaginationComponent={Pagination}
                                    data={this.state.data_l}
                                    columns={this.state.columns_l}
                                    onColumnUpdate={this.toggleColumnChooser}
                                    loading={isLoading}
                                    {...checkboxProps}
                                    defaultPageSize={5}
                                    // style={{
                                    //     height: "480px" // This will force the table body to overflow and scroll, since there is not enough room
                                    // }}
                                    className="-striped -highlight"
                                    defaultSorted={[
                                        {
                                            id: "id",
                                            desc: true
                                        }
                                    ]}
                                />
                            </CardBody>
                        </Card>
                    </GridItem>
                </GridContainer>
            </>
        );
    }
}